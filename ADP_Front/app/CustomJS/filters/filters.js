appDashboard.filter("convertDDHHMM", function () {
        return function (input, suffix) {
            return convert2ddhhmm(input, suffix);
        }
    })
.filter('titleCase', function () {
        return function (input) {
            input = input || '';
            return input.replace(/\w\S*/g, function (txt) {
                return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
            });
        };
    });
