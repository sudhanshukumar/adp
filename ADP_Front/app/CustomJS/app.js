var MakePriorityBasedData1 = {};
var MakePriorityBasedNumber1 = {};
var ParetoData = [];
var uriApiURL = "";
/**/
//var uriApiURL = "/";

var hrefIs = window.location.href;
var sHrefIs = window.location.protocol;
var hostIsG = window.location.host
var hostIs = window.location.hostname;
var portIs = window.location.port;
var pathIs = window.location.pathname;
var searchIs = window.location.search;
var routeIs = window.location.hash;
var pathArray = window.location.pathname.split('/');
var pathAbs;
var pathAbs0 = pathArray[1];
if (portIs == "") {
    uriApiURL = sHrefIs + "//" + hostIs + window.location.pathname + "";
} else {
    uriApiURL = sHrefIs + "//" + hostIs + ":" + portIs + window.location.pathname + "";
}
//

var uriApiURL = "http://localhost:55454/";
//uriApiURL = "http://172.25.13.28/MosaicDiscoveryPlatform/";
console.log(uriApiURL   );
var appDashboard = angular.module('dashboard', ['angular-svg-round-progress', 'gridster', 'ngTagsInput', 'angular-jqcloud', 'ui.sortable', 'ui.bootstrap', 'ngRoute', "angular-multi-select", "ui-notification", 'ngSanitize', 'angular-intro', 'colorpicker.module', 'LocalStorageModule', 'dndLists', 'angular-carousel', 'slickCarousel', 'mb-scrollbar', 'ngCsv', 'daterangepicker']);
appDashboard.config(['$routeProvider', '$httpProvider', function ($routeProvider, $httpProvider) {

    $httpProvider.defaults.useXDomain = true;
    delete $httpProvider.defaults.headers.common['X-Requested-With'];
    $routeProvider
	 .when('/', {
	     templateUrl: 'app/views/_Home.html',
	     controller: 'DashboardListController'
	 })
	 .when('/assessment/:AssessmentId', {
	     templateUrl: 'app/views/_assessment.html',
	     controller: 'AssessmentIdController'
	 })
    .when('/login', {
        templateUrl: 'app/views/_LoginPage.html',
        controller: 'ApLoginController'

    })
    .when('/settings', {
        templateUrl: 'app/views/settings/dashboard.html',
	    controller: 'dashSettingCtrl'
	})
   .when('/admin', {
       templateUrl: 'app/views/_Admin.html',
        controller: 'AdminController'
    })
    .when('/proc/:AssessmentId', {
        templateUrl: 'app/views/_Proc.html',
        controller: 'ProcController'
    })
        .when('/appSettings', {
            templateUrl: 'app/views/settings/appsettings.html',
	    controller: 'appSettingCtrl'
	})
    .when('/:AssessmentId/dashboard/:dashboardId', {
        templateUrl: 'app/views/_Dashboard.html',
        controller: 'KPIListController'
    })
    .when('/Search', {
        templateUrl: 'app/views/_FilterPopup.html',
        controller: 'FilterPopupController'
    })
        .when('/Upload', {
            templateUrl: 'app/views/_UploadData.html',
            controller: 'UploadDataController'
        })
   
    .when('/MasterColumnSettings/:AssessmentId', {
        templateUrl: 'app/views/settings/_MasterColMap.html',
        controller: 'MasterColMapController'
    })
    .when('/BaseKPIColMap/:AssessmentId', {
        templateUrl: 'app/views/settings/_BaseKPIColMap.html',
        controller: 'BaseKPIColMapController'
    })
    .when('/Cluster/:AssessmentId', {
        templateUrl: 'app/views/_Cluster.html',
        controller: 'ClusterController'
    })
    .when('/PreProcess/:AssessmentId', {
        templateUrl: 'app/views/_PreProcess.html',
        controller: 'PreProcessController'
    })
    .when('/RequestDrillDown', {
        templateUrl: 'app/views/_RequestDrillDown.html',
        controller: 'RequestDrillDownController'
    })
    .when('/treeMapping', {
        templateUrl: 'app/views/KPI/TreeMapping.html',
        controller: 'treeMappingController'
    })
    .when('/RequestDrillDown/:RequestId', {
        templateUrl: 'app/views/_RequestDrillDown.html',
        controller: 'RequestDrillDownController'
    })
    .otherwise({
        redirectTo: '/'
    });
    // use the HTML5 History API
    //$locationProvider.html5Mode(true);
}]);

appDashboard.run(function ($rootScope, $location, $route, $window, $http, $uibModalStack, mySharedService) {
    $rootScope.showHeader = true;
    $rootScope.UserInfo = "";
    $rootScope.UserInfo.userID = "onstart";
    angular.element($window).bind('resize', function () {
        console.log($window.innerWidth);
    });
    $rootScope.$on('$routeChangeStart', function () {

        if (($rootScope.UserInfo.userID == "null") || ($rootScope.UserInfo.userID == null)) {
            if ($location.path() != "/login") {
                console.log("USR DETAIL API Check " + $rootScope.UserInfo.userID);
                var epoTsmp = getEpocStamp();
                $http({ method: 'GET', url: uriApiURL + "api/Dashboard/GetUserDetails?version=" + epoTsmp })
                .then(function (response) {
                    $rootScope.UserInfo = angular.fromJson(response.data);
						console.log("AAa " + $rootScope.UserInfo.isLoginType + " dd "  +$rootScope.UserInfo.isAuth)
						if(  ($rootScope.UserInfo.isLoginType == "1")&& ($rootScope.UserInfo.isAuth == false)){
								console.log("rootScope redirect")
								 $window.location.href = $rootScope.UserInfo.redirectURL+"?returl=" +uriApiURL ;
							  
						}
                    if (($rootScope.UserInfo.userID == "null") || ($rootScope.UserInfo.userID == null)) {
                        //	$location.path('/login');
                    } else {
                        console.log("$rootScope.UserInfo" + $rootScope.UserInfo);
                        //localStorage.setItem( appLocStrUsrVar, response.data);
                        $rootScope.IsAdmin = $rootScope.UserInfo.adminDet.isAdmin;
                        sessionStorage.UserInfo = JSON.stringify($rootScope.UserInfo);
                        mySharedService.StartDateAvailable = $rootScope.UserInfo.startRecordTimeStamp;
                        mySharedService.EndDateAvailable = $rootScope.UserInfo.lastModifiedDate;
                        mySharedService.SetUserDetail(sessionStorage.UserInfo);

                    }
                })
            }

        }

    });
    $rootScope.$on('$routeChangeSuccess', function () {
        if (Object.getPrototypeOf($route.current) === $route.routes['/login']) {
            $rootScope.showHeader = false;
        } else {
            $rootScope.showHeader = true;
        }
    });
})