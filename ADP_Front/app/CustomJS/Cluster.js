﻿appDashboard.controller('ClusterController', function ($scope,$rootScope,limitToFilter, $window, $location,$timeout,Notification, $uibModal, $http, $routeParams, mySharedService) {
    $scope.AssessmentId = $routeParams.AssessmentId;
    var AssessmentVAl = [];
    var steps = 6;
    $scope.IsResendData = 0;
    $scope.TckAccepted = 0;
    $scope.TotalClusterRows = 0;
    $scope.TckReCluster = 0;
    $scope.TckUnCategorized = 0;
    $scope.ISClusterDataAvail = true;
    $scope.ISClusterDataNotAvail = true;
    $scope.IsClusterTableAvail = false;
    $scope.IsClusterTableNotAvail = false;
    var IsNxt=false;
    var dropDownValues = [], AssignmentData = [];
    var items = [];
    $scope.colors = generateColorGradient('', $scope.ATClr, steps);
    $scope.colors.reverse();
    $scope.steps = steps;
    dropDownValues = ['Accept Clustering', 'Send for Re-clustering', 'Mark as Uncategorized'];
    var uniqueNames = [];
    $scope.WholeClusterData = [];
    var GetWordCloud = [], val = [];
    if ($scope.ATClr == undefined) {
        $scope.ATClr="cyan"
    }
    $scope.onSelect = function (item) {
        $scope.TagName = item;
        $scope.ChangeTagname();
    }
    $scope.MFGetCluster = function (VsVal,type) {
        $scope.isLoadAsgData = false;
        $scope.isLoadClusterData = false;
        $http.get(uriApiURL + "api/Proffer/GetAssesmentDetails?AsgID=" + $scope.AssessmentId + "&VerVal="+VsVal+"&IsType=1")
        .then(function (response) {
            if (angular.fromJson(response.data) != null) {
                $scope.ClusterWholeData = angular.fromJson(angular.fromJson(response.data).AsgJson).WholeData;
                $scope.ISClusterDataAvail = true;
                $scope.ISClusterDataNotAvail = false;
                $scope.ClusterWholeData.sort(function (a, b) {
                    return parseInt(a.ClusterData.length) - parseInt(b.ClusterData.length);
                });
                $scope.ClusterWholeData.reverse();
                for (var i = 0; i < $scope.ClusterWholeData.length; i++) {
                    $scope.WholeClusterData.push($scope.ClusterWholeData[i]);
                    if (uniqueNames.indexOf($scope.ClusterWholeData[i].AssignmentId) === -1) {
                        uniqueNames.push($scope.ClusterWholeData[i].AssignmentId);
                        AssignmentData.push({ AssignmentId: '' + $scope.ClusterWholeData[i].AssignmentId + '', IsCluster: '', ClusterTag: '', ISRecluster: '' })
                    }
                }
                var TempAsgNo = [];
                for (var i = 0; i < uniqueNames.length; i++) {
                    TempAsgNo.push({ name: uniqueNames[i] });
                    
                }
                $scope.AssignmntNos = [];
                $scope.AssignmntNos = TempAsgNo;
            } else {
                $scope.ISClusterDataAvail = false;
                $scope.ISClusterDataNotAvail = true;
            }
            $scope.isLoadClusterData = true;
            $scope.isLoadAsgData = true;
            
            
        });
    }
  
    $scope.FilterTckData = function (val, type) {
        var currentpos
        if (type == 0) {
            currentpos = uniqueNames.indexOf($scope.selectedAssignment);
        } else {
            currentpos = uniqueNames.indexOf($scope.SelAssigmentId.name);
        }        
        $scope.selectedAssignment = uniqueNames[currentpos];
        if (val == 0) {
            $scope.TckTypeValue = "Accept Clustering"
        } else if (val == 1) {
            $scope.TckTypeValue = "Mark as Uncategorized"
        } else if (val == 2) {
            $scope.TckTypeValue = "Send for Re-clustering"
        }
        else {
            $scope.TckTypeValue = "All";
        }
        var WholeData = GetFilterValWitTck($scope.selectedAssignment, 'AssignmentId', $scope.WholeClusterData, $scope.TckTypeValue);
        $scope.TotalRows = WholeData.length;
        $scope.GetMasterTable(WholeData);
        $scope.SaveClusterProcData($scope.WholeClusterData, $scope.TckTypeValue, WholeData);
    }
    $scope.SaveClusterProcData = function (Wholedt,type,changdt) {
        $scope.WholeClusterData = Wholedt;
        var wholeitem = $.grep($scope.WholeClusterData, function (obj) {
            if (obj.AssignmentId != $scope.selectedAssignment)
                return obj;
        })
        var v = $.grep($scope.WholeClusterData, function (obj) {
            if (obj.AssignmentId == $scope.selectedAssignment)
                return obj;
        })
        v[0].ClusterData = $.grep(v[0].ClusterData, function (obj) {
            if ((obj.Disposition != type && type!="All"))
                return obj;
        })
        for (var t = 0; t < changdt.length; t++) {
            v[0].ClusterData.push(changdt[t]);
        }
        wholeitem.push(v[0]);
        $scope.WholeClusterData = wholeitem;
    }
    $scope.MFGetCluster(0,1);
    $scope.UploadCluster = function () {
        $scope.ExportFname="Request_Processed_Details"
        $scope.DownlData = [];
        $scope.DownlData.push({
            "Request_Id": "",
            "Description":"",
            "Proc_Description": "",
            "Assignments": "",
            "Tag_Name": ""
        });
        $scope.IsUpload = 1;
        $scope.IsAgsUpload = false;
        $scope.urlUpload = "api/Proffer/UploadClusterData?AsgID=" + $scope.AssessmentId+"&ClustName=";
        $scope.HeaderName = " Assesment Data";
        var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: 'app/views/_UploadModal.html',
            controller: 'UploadModalController',
            backdrop: 'static',
            size: 'md large-model',//size
            scope: $scope
        })
    }
    $scope.$on('GetClusterDataMain', function () {
        var tab = ($scope.ActTab == 4) ? $scope.ActTab : $scope.ActTab + 1;
        $scope.ChangeStep(4, tab);
        $scope.ISClusterDataAvail = true;
        $scope.ISClusterDataNotAvail = false;
    })
    $scope.OnSelectAsgId = function ()
    {
        $scope.TckAccepted = 0;
        $scope.TckReCluster = 0;
        $scope.TckUnCategorized = 0;
        $scope.isLoadClusterData = false;        
        if ($('#masterTable1').handsontable('getInstance') != undefined) {
            IsNxt = false;
            $scope.WholeClusterData = SaveData($scope.WholeClusterData, 1)
        }
        if (IsNxt == true) {
            Notification.error({ message: 'Enter Tag name' });
            var selAs = $.grep($scope.AssignmntNos, function (obj) {
                if (obj.name == $scope.selectedAssignment)
                    return obj;
            })
            $scope.SelAssigmentId = selAs[0];
        } else {
            $scope.ClusterData = false;
            $scope.ClusteringNo = false;
            $scope.ClusteringYes = false;
            $scope.selectedAssignment = $scope.SelAssigmentId.name;
            var currentpos = uniqueNames.indexOf($scope.selectedAssignment);
            if (currentpos == 0) {
                $scope.Nextbtn = true;
                $scope.Prevbtn = false;
            } else if (currentpos == uniqueNames.length - 1) {
                $scope.Nextbtn = false;
                $scope.Prevbtn = true;
            } else {
                $scope.Nextbtn = true;
                $scope.Prevbtn = true;
            }
            var WholeData = GetFilterVal($scope.selectedAssignment, 'AssignmentId', $scope.WholeClusterData);
            var WordCloudData = WholeData[0].WordCloud;
            var FilterVal = WholeData[0].ClusterData;
            $scope.TotalClusterRows = FilterVal.length;
            items = FilterVal;
            $scope.ClusterData = true;
            var text = "";
            for (var i = 0; i < FilterVal.length; i++) {
                if (FilterVal[i].Disposition == "Accept Clustering") {
                    $scope.TckAccepted++;
                }
                else if (FilterVal[i].Disposition == "Send for Re-clustering") {
                    $scope.TckReCluster++;
                }
                else if (FilterVal[i].Disposition == "Mark as Uncategorized") {
                    $scope.TckUnCategorized++;
                }
                if (FilterVal[i].Disposition == undefined) {
                    FilterVal[i].Disposition = "";
                }
            }
            $scope.TotalRows = FilterVal.length;
            $scope.GetMasterTable(FilterVal);
            $('#demo').empty();
            $('#demo').jQCloud('destroy');
            $timeout(function () {
                $('#demo').jQCloud(WordCloudData, {
                    colors: $scope.colors,
                    autoResize: true
                });
            });
$scope.$on('setBaseColor', function (e, Aclr, Bclr) {
            });

            AssessmentVAl = ResetAndGiveVal();
            $scope.ISRecluster=AssessmentVAl[0].ISRecluster;
	    $scope.TagName=AssessmentVAl[0].ClusterTag;
            $scope.IsCluster=AssessmentVAl[0].IsCluster;
            if($scope.IsCluster=='0'){
	       $scope.ClusteringYes=true;
	       $scope.ClusteringNo=false;
	    }else if($scope.IsCluster=='1'){
	       $scope.ClusteringYes=false;
	       $scope.ClusteringNo=true;
            }
        }
        $scope.isLoadClusterData = true;
    }
    $scope.ChangeIsCluster = function (value) {
        $scope.isLoadClusterData = false;
        AssessmentVAl[0].IsCluster = value;
        $scope.ISRecluster = 2;
        if (value == 0) {
            $scope.ClusteringNo = false;
            $scope.ClusteringYes = true;
            ht = $("#masterTable1").handsontable("getInstance");
            var update = [];
            AssessmentVAl[0].ISRecluster = '0';
            var data = dropDownValues[0];
            var rows = ht.countRows();
            for (i = 0; i < rows; i++) {
                update.push([i, 7, data]);
            }
            $scope.TckAccepted = 0;
	    $scope.TckReCluster = 0;
            $scope.TckUnCategorized = 0;
            ht.setDataAtCell(update);
            ht.render();
            $('#masterTable1').handsontable('updateSettings', {
                readOnly: false
            });
            $scope.getsuggesstion = function (sug) {
                return $http.get(uriApiURL + "api/Proffer/GetTagName?AsgId="+ $scope.AssessmentId +"&Keyword=" + sug, {
                    SearchJson: sug
                })
                    .then(function (response) {
                        var WholeData = angular.fromJson(response.data.m_StringValue);
                        return limitToFilter(WholeData.TagDetails, 10);
                    });
            };
        }
        else {
            ht = $("#masterTable1").handsontable("getInstance");
            var update = [];
            $scope.TagName = "";
            AssessmentVAl[0].ISRecluster = '0';
            var data = dropDownValues[0];
            var rows = ht.countRows();
            for (i = 0; i < rows; i++) {
                update.push([i, 6, ""]);
                update.push([i, 7, ""]);
            }
            ht.setDataAtCell(update);
            ht.render();
            $scope.TckAccepted = 0;
	    $scope.TckReCluster = 0;
            $scope.TckUnCategorized = 0;
            $scope.ClusteringNo = true;
            $scope.ClusteringYes = false;
        }
        $scope.isLoadClusterData = true;
    }
    $scope.ChangeIsRecluster = function (value) {
        $scope.TckAccepted = 0;
        $scope.TckReCluster = 0;
        $scope.TckUnCategorized = 0;
        AssessmentVAl[0].IsCluster = 1
        $scope.IsWholeChangeVal=1
        $scope.isLoadClusterData = false;
        if (value == '0') {
            ht = $("#masterTable1").handsontable("getInstance");
            var update = [];
            AssessmentVAl[0].ISRecluster = '0';
            var data = dropDownValues[1];
            var rows = ht.countRows();
            for (i = 0; i < rows; i++) {
                update.push([i, 7, data]);
                //$scope.TckReCluster++;
            }
            ht.setDataAtCell(update);
            ht.render();
        }
        else {
            ht = $("#masterTable1").handsontable("getInstance");
            var update = [];
            AssessmentVAl[0].ISRecluster = '1';
            var data = dropDownValues[2];
            var rows = ht.countRows();
            for (i = 0; i < rows; i++) {
                update.push([i, 7, data]);               
            }
            ht.setDataAtCell(update);
            ht.render();
        }
        $scope.isLoadClusterData = true;
    }
    $scope.ChangeTagname = function () {
        AssessmentVAl[0].ClusterTag = $scope.TagName
        ht = $("#masterTable1").handsontable("getInstance");
        var update = [];
        var data = $scope.TagName;
        var rows = ht.countRows();
        for (i = 0; i < rows; i++) {
            update.push([i, 6, data]);
            update.push([i, 7, dropDownValues[0]]);
        }
        ht.setDataAtCell(update);
        ht.render();
    }
    $scope.Next = function () {
        $scope.TckAccepted = 0;
        $scope.TckReCluster = 0;
        $scope.TckUnCategorized = 0;
        $scope.isLoadClusterData = false;
        var currentpos = uniqueNames.indexOf($scope.SelAssigmentId.name);
        var nextassignmentid = currentpos + 1;
        if ($('#masterTable1').handsontable('getInstance') != undefined) {
            IsNxt = false;
            $scope.WholeClusterData = SaveData($scope.WholeClusterData, 0);
            if (IsNxt == true) {
                Notification.error({ message: 'Enter Tag name' });
            }
            else {
                $scope.IsCluster = 2;
                $scope.ISRecluster = 2;
                $scope.TagName = "";
                $scope.ClusteringNo = false;
                $scope.ClusteringYes = false;
                $scope.selectedAssignment = uniqueNames[currentpos + 1];
                var WholeData = GetFilterVal($scope.selectedAssignment, 'AssignmentId', $scope.WholeClusterData);
                var WordCloudData = WholeData[0].WordCloud;
                var FilterVal = WholeData[0].ClusterData;
                $scope.TotalClusterRows = FilterVal.length;
                items = FilterVal;
                $scope.ClusterData = true;
                var text = "";
                for (var i = 0; i < FilterVal.length; i++) {
                    if (FilterVal[i].Disposition == "Accept Clustering")
                    {
                        $scope.TckAccepted++;
                    }
                    else if (FilterVal[i].Disposition == "Send for Re-clustering") {
                        $scope.TckReCluster++;
                    }
                    else if (FilterVal[i].Disposition == "Mark as Uncategorized") {
                        $scope.TckUnCategorized++;
                    }
                    if (FilterVal[i].Disposition == undefined) {
                        FilterVal[i].Disposition = "";
                    }
                }
                $scope.TotalRows = FilterVal.length;
                $scope.GetMasterTable(FilterVal);
                $('#demo').empty();
                $('#demo').jQCloud('destroy');
                $timeout(function () {
                    $('#demo').jQCloud(WordCloudData, {
                        colors: $scope.colors,
                        autoResize: true
                    });
                });
 $scope.$on('setBaseColor', function (e, Aclr, Bclr) {
    });
                if (currentpos + 1 == uniqueNames.length - 1) {
                    $scope.Nextbtn = false;
                    $scope.Prevbtn = true;
                } else {
                    $scope.Nextbtn = true;
                    $scope.Prevbtn = true;

                }
                var selAs = $.grep($scope.AssignmntNos, function (obj) {
                    if (obj.name == $scope.selectedAssignment)
                        return obj;
                })
                $scope.SelAssigmentId = selAs[0];
                AssessmentVAl = ResetAndGiveVal();
                $scope.ISRecluster=AssessmentVAl[0].ISRecluster;
		$scope.TagName=AssessmentVAl[0].ClusterTag;
                $scope.IsCluster=AssessmentVAl[0].IsCluster;
                if($scope.IsCluster=='0'){
                    $scope.ClusteringYes=true;
                    $scope.ClusteringNo=false;
                }else if($scope.IsCluster=='1'){
                    $scope.ClusteringYes=false;
                    $scope.ClusteringNo=true;
                }
            }
        }
        $scope.isLoadClusterData = true;
    }
    $scope.Previous = function ()
    {
        $scope.TckAccepted = 0;
        $scope.TckReCluster = 0;
        $scope.TckUnCategorized = 0;
        $scope.isLoadClusterData = false;
        var currentpos = uniqueNames.indexOf($scope.SelAssigmentId.name);
        var nextassignmentid = currentpos - 1;
        if ($('#masterTable1').handsontable('getInstance') != undefined) {
            IsNxt = false;
            $scope.WholeClusterData = SaveData($scope.WholeClusterData, 0)
            if (IsNxt == true) {
                Notification.error({ message: 'Enter Tag name' });
            }
            else {
                $scope.ClusterData = false;
                $scope.IsCluster = 2;
                $scope.ISRecluster = 2;
                $scope.TagName = "";
                $scope.ClusteringNo = false;
                $scope.ClusteringYes = false;
                $scope.selectedAssignment = uniqueNames[currentpos - 1];;
                var WholeData = GetFilterVal($scope.selectedAssignment, 'AssignmentId', $scope.WholeClusterData);
                var WordCloudData = WholeData[0].WordCloud;
                var FilterVal = WholeData[0].ClusterData;
                $scope.TotalClusterRows = FilterVal.length;
                items = FilterVal;
                $scope.ClusterData = true;
                var text = "";
                for (var i = 0; i < FilterVal.length; i++) {
                    if (FilterVal[i].Disposition == "Accept Clustering") {
                        $scope.TckAccepted++;
                    }
                    else if (FilterVal[i].Disposition == "Send for Re-clustering") {
                        $scope.TckReCluster++;
                    }
                    else if (FilterVal[i].Disposition == "Mark as Uncategorized") {
                        $scope.TckUnCategorized++;
                    }
                    if (FilterVal[i].Disposition == undefined) {
                        FilterVal[i].Disposition = "";
                    }
                }
                $scope.TotalRows = FilterVal.length;
                $scope.GetMasterTable(FilterVal);
                $('#demo').empty();
                $('#demo').jQCloud('destroy');
                $timeout(function () {
                    $('#demo').jQCloud(WordCloudData, {
                         colors: $scope.colors,
                        autoResize: true
                    });
                });
  		$scope.$on('setBaseColor', function (e, Aclr, Bclr) {
            });
                if (currentpos == 1) {
                    $scope.Nextbtn = true;
                    $scope.Prevbtn = false;
                }
                else {
                    $scope.Nextbtn = true;
                    $scope.Prevbtn = true;

                }
                var selAs = $.grep($scope.AssignmntNos, function (obj) {
                    if (obj.name == $scope.selectedAssignment)
                        return obj;
                })
                $scope.SelAssigmentId = selAs[0];
                AssessmentVAl = ResetAndGiveVal();
                $scope.ISRecluster=AssessmentVAl[0].ISRecluster;
                $scope.TagName=AssessmentVAl[0].ClusterTag;
                $scope.IsCluster=AssessmentVAl[0].IsCluster;
                if($scope.IsCluster=='0'){
		    $scope.ClusteringYes=true;
		    $scope.ClusteringNo=false;
		}else if($scope.IsCluster=='1'){
		    $scope.ClusteringYes=false;
		    $scope.ClusteringNo=true;
                }
            }
        }
        $scope.isLoadClusterData = true;
    }
    $scope.ResendCluster = function () {
        if ($('#masterTable1').handsontable('getInstance') != undefined) {
            $scope.WholeClusterData = SaveData($scope.WholeClusterData)
        }
        var TestClusterJSON = [];
        for (var i = 0; i < $scope.WholeClusterData.length; i++) {
            for (var j = 0; j < $scope.WholeClusterData[i].ClusterData.length; j++) {
                if ($scope.WholeClusterData[i].ClusterData[j].Disposition == "Send for Re-clustering")
                    TestClusterJSON.push({
                        "ASG_ID": $scope.AssessmentId,
                        "TAG": ($scope.WholeClusterData[i].ClusterData[j].Tags != null && $scope.WholeClusterData[i].ClusterData[j].Tags != undefined) ? $scope.WholeClusterData[i].ClusterData[j].Tags : '',
                        "DISPOSITION": ($scope.WholeClusterData[i].ClusterData[j].Disposition != null && $scope.WholeClusterData[i].ClusterData[j].Disposition != undefined) ? $scope.WholeClusterData[i].ClusterData[j].Disposition : '',
                        "CLUSTER_ID": ($scope.WholeClusterData[i].ClusterData[j].asg != undefined ? $scope.WholeClusterData[i].ClusterData[j].asg : $scope.WholeClusterData[i].ClusterData[j].AssignmentId),
                        "REQUEST_ID": $scope.WholeClusterData[i].ClusterData[j].tn,
                        "PROC_DESCRIPTION": $scope.WholeClusterData[i].ClusterData[j].pd
                    })
            }
        }
        if (TestClusterJSON.length != 0) {
            mySharedService.ResendClusterData = TestClusterJSON;
            mySharedService.AssessmentId = $scope.AssessmentId;
            $scope.IsResendData = 1;
            var modalInstance = $uibModal.open({
                animation: $scope.animationsEnabled,
                templateUrl: 'app/views/_StartPreProcModal.html',
                controller: 'StartPreprocModalController',
                backdrop: 'static',
                size: 'md large-model',//size
                scope: $scope
            })
        } else {
            Notification.error({message:'No data to send for Reclustering'});
        }
    }
    $scope.$on("SaveClusterData", function (val) {
        $scope.SaveClusterData(1);        
    })
    $scope.SetAsgData = function () {
        $http.get(uriApiURL + "api/Proffer/SetAsgDataProc?AsgID=" + $scope.AssessmentId + "&Condition=2&Type=1")
           .then(function (response) {
               var tab = ($scope.ActTab == 4) ? $scope.ActTab : $scope.ActTab + 1;
               $scope.ChangeStep(2, tab);
           })
    }
    $scope.SaveClusterData = function (value) {
        $scope.isLoadClusterData = false;
        if ($('#masterTable1').handsontable('getInstance') != undefined) {
            $scope.WholeClusterData = SaveData($scope.WholeClusterData)
        }
        var TestClusterJSON = [];
        for (var i = 0; i < $scope.WholeClusterData.length; i++) {
            for (var j = 0; j < $scope.WholeClusterData[i].ClusterData.length; j++) {
                if ($scope.WholeClusterData[i].ClusterData[j].Disposition == "Mark as Uncategorized") {
                    $scope.WholeClusterData[i].ClusterData[j].Tags = "UnCategorized";
                    $scope.WholeClusterData[i].ClusterData[j].Disposition = "Accept Clustering";
                }
                TestClusterJSON.push({
                    "ASG_ID": $scope.AssessmentId,
                    "Tag_Name": ($scope.WholeClusterData[i].ClusterData[j].Tags != null && $scope.WholeClusterData[i].ClusterData[j].Tags != undefined) ? $scope.WholeClusterData[i].ClusterData[j].Tags : '',
                    "Disposition": ($scope.WholeClusterData[i].ClusterData[j].Disposition != null && $scope.WholeClusterData[i].ClusterData[j].Disposition != undefined) ? $scope.WholeClusterData[i].ClusterData[j].Disposition : '',
                    "Assignments":($scope.WholeClusterData[i].ClusterData[j].asg != undefined ? $scope.WholeClusterData[i].ClusterData[j].asg : $scope.WholeClusterData[i].ClusterData[j].AssignmentId),
                    "Request_Id": $scope.WholeClusterData[i].ClusterData[j].tn,
                    "Proc_Description": $scope.WholeClusterData[i].ClusterData[j].pd
                })
            }
        }
        var ClusterData = TestClusterJSON;
        $.ajax({
            type: "POST",
            url: uriApiURL + "api/Proffer/SaveClusterData?AsgID=" + $scope.AssessmentId + "&IsResendData=" + $scope.IsResendData,
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            data: JSON.stringify(ClusterData),
            success: function (response) {
                $scope.isLoadClusterData = true;
                $rootScope.ActDrillVal = [];
                $rootScope.SndDrillVal = [];
                $rootScope.MarkDrillVal = [];
                $rootScope.NotClusteredVal = [];
                Notification.success({ message: 'Saved Successfully' });
                if (value == 1) {
                    $scope.SetAsgData();
                }
            }
        });
    }    
    function GetFilterValWitTck(id, propname, clusterArray, type) {
        var arrayList = [];
        for (var i = 0; i < clusterArray.length; i++) {
            if (clusterArray[i]['' + propname + ''] == id) {
                for (var j = 0; j < clusterArray[i].ClusterData.length; j++) {
                    if (type != "All") {
                        if (clusterArray[i].ClusterData[j].Disposition == type) {
                            arrayList.push(clusterArray[i].ClusterData[j]);
                        }
                    } else {
                        arrayList.push(clusterArray[i].ClusterData[j]);
                    }
                }
            }
        }
        return arrayList;
    }
    function GetFilterVal(id, propname, clusterArray) {
        var arrayList = [];
        for (var i = 0; i < clusterArray.length; i++) {
            if (clusterArray[i]['' + propname + ''] == id) {
                arrayList.push(clusterArray[i]);
            }
        }
        return arrayList;
    }
    function GetTableHeight() {
        var setTableHeight = window.innerHeight - 250;
        return setTableHeight;
    }
    function SaveData(WholeClusterData,IsNxtPrev) {
        var t = [];
        $scope.WholeClusterData = WholeClusterData;
        var wholeitem = $.grep($scope.WholeClusterData, function (obj) {
            if (obj.AssignmentId != $scope.selectedAssignment)
                return obj;
        })
        var v = $.grep($scope.WholeClusterData, function (obj) {
            if (obj.AssignmentId == $scope.selectedAssignment)
                return obj;
        })
        if ($scope.TckTypeValue != "All" && IsNxtPrev==1) {
            $scope.FilterTckData(3,0);
        } else if($scope.TckTypeValue != "All"){
            $scope.FilterTckData(3);
        }
        var $container = $('#masterTable1');
        var ht = $container.handsontable('getInstance');
        var htContents = ht.getData();
        for (var i = 0; i < htContents.length; i++) {
            if (IsNxtPrev == 0 || IsNxtPrev==1)  {
                if (htContents[i][7] == "Accept Clustering" && (htContents[i][6] == "" || htContents[i][6] == null)) {
                    IsNxt = true;
                }
            }
            t.push({
                tn: "" + htContents[i][0] + "",
                ct1: "" + htContents[i][1] + "",
                ct2: "" + htContents[i][2] + "",
                ct3: "" + htContents[i][3] + "",
                sd: "" + htContents[i][4] + "",
                pd:""+htContents[i][5]+"",
                Tags: "" + (htContents[i][6] != null) ? htContents[i][6] : '' + "", Disposition: '' + htContents[i][7] + '', AssignmentId: $scope.selectedAssignment
            })
            //items.push({ Key: "" + htContents[i][0] + "", label_column: "" + htContents[i][1] + "", text_column: "" + htContents[i][2] + "", Tags: "" + (htContents[i][3] != null) ? htContents[i][3] : '' + "", Disposition: '' + htContents[i][4] + '', Assignments: $scope.selectedAssignment })
        }
        v[0].ClusterData = t;
        wholeitem.push(v[0]);
        $scope.WholeClusterData = wholeitem;
        return $scope.WholeClusterData;
    }
    function ResetAndGiveVal() {
        $scope.IsCluster = 2;
        $scope.ISRecluster = 2;
        if(AssessmentVAl.length==0){
            AssessmentVAl = $.grep(AssignmentData, function (obj) {
       		if (obj.AssignmentId == $scope.selectedAssignment)
        	        return obj;
            })
        }
        var CheckRadioVal = AssessmentVAl[0].IsCluster;
        if (CheckRadioVal != '') {
            $scope.IsCluster = parseInt(CheckRadioVal);
            if (CheckRadioVal == 0) {
                $scope.ClusteringNo = false;
                $scope.ClusteringYes = true;
            } else {
                $scope.ClusteringNo = true;
                $scope.ClusteringYes = false;
            }
        }
        if (CheckRadioVal == 0) {
            $scope.TagName = AssessmentVAl[0].ClusterTag;
        } else if (CheckRadioVal == 1) {
            if (AssessmentVAl[0].ISRecluster != '') {
                $scope.ISRecluster = parseInt(AssessmentVAl[0].ISRecluster);
            }
        }
        return AssessmentVAl;
    }
    $scope.GetMasterTable = function (data) {
        if (data.length != 0) {
            $scope.IsClusterTableAvail = true;
            $scope.IsClusterTableNotAvail = false;            
            if ($('#masterTable1').handsontable('getInstance') != undefined) {
                $("#masterTable1").handsontable('destroy');
            }
            $('#masterTable1').handsontable({
                data: data,
                colHeaders: ['Ticket No', 'Category Tier 1', 'Category Tier 2', 'Category Tier 3', 'Description', 'P', 'Tags', 'Disposition'],
                columnHeaderHeight: 20,
                height: 350,
                rowHeight: function (row) {
                    return 30;
                },
                defaultRowHeight: 30,
                columnSorting: true,
                manualColumnResize: true,
                sortIndicator: true,
                stretchH: "all",
                colWidths: [110, 130, 130, 130, 250, 1, 150, 150],
                columns: [
                    {
                        data: 'tn',
                        readOnly: true,

                    },
                    {
                        data: 'ct1',
                        readOnly: true
                    },
                    {
                        data: 'ct2',
                        readOnly: true
                    },
                    {
                        data: 'ct3',
                        readOnly: true
                    },
                    {
                        data: 'sd',
                        readOnly: true
                    },
                    {
                        data: 'pd',
                        readOnly: true
                    },
                    {
                        data: 'Tags'
                    },
                    {
                        editor: 'select',
                        selectOptions: dropDownValues,
                        data: 'Disposition'
                    }
                ],
                afterChange: function (changes, source) {
                    if (source == "edit" || source == "paste" || source == "undo" || source == "autofill") {
                        $timeout(function () {
                            for (var i = 0; i < changes.length; i++) {
                                if (changes[i][1] == "Disposition") {
                                    var refId = $('#masterTable1').handsontable('getDataAtCell', changes[i][0], "Disposition");
                                    var tags = $('#masterTable1').handsontable('getDataAtCell', changes[i][0], "Tags");
                                    var pd = $('#masterTable1').handsontable('getDataAtCell', changes[i][0], "pd");
                                    var sd = $('#masterTable1').handsontable('getDataAtCell', changes[i][0], "sd");
                                    var ct3 = $('#masterTable1').handsontable('getDataAtCell', changes[i][0], "ct3");
                                    var ct2 = $('#masterTable1').handsontable('getDataAtCell', changes[i][0], "ct2");
                                    var ct1 = $('#masterTable1').handsontable('getDataAtCell', changes[i][0], "ct1");
                                    var tn = $('#masterTable1').handsontable('getDataAtCell', changes[i][0], "tn");
                                    if (refId == "Accept Clustering" && tags != "UnCategorized") {
                                        $scope.TckAccepted++;
                                        $rootScope.AcceptTck++;
                                        var TmpJs = { tn: tn, ct1: ct1, ct2: ct2, ct3: ct3, sd: sd, pd: pd, Tags: tags, Disposition: refId,IsTk:0 };
                                        $rootScope.ActDrillVal.push(TmpJs);
                                    } else if (refId == "Send for Re-clustering") {
                                        $scope.TckReCluster++;
                                        $rootScope.ReClustTck++;
                                        var TmpJs = { tn: tn, ct1: ct1, ct2: ct2, ct3: ct3, sd: sd, pd: pd, Tags: tags, Disposition: refId, IsTk: 0 };
                                        $rootScope.SndDrillVal.push(TmpJs);
                                    } else if (refId == "Mark as Uncategorized" || (refId == "Accept Clustering" && tags == "UnCategorized")) {
                                        $scope.TckUnCategorized++;
                                        $rootScope.UnCatTck++;
                                        var TmpJs = { tn: tn, ct1: ct1, ct2: ct2, ct3: ct3, sd: sd, pd: pd, Tags: tags, Disposition: refId, IsTk: 0 };
                                        $rootScope.MarkDrillVal.push(TmpJs);
                                    } else if (refId == "") {
                                        $rootScope.NotClustered++;
                                        var TmpJs = { tn: tn, ct1: ct1, ct2: ct2, ct3: ct3, sd: sd, pd: pd, Tags: tags, Disposition: refId, IsTk: 0 };
                                        $rootScope.NotClusteredVal.push(TmpJs);
                                    }
                                    if (changes[i][2] == "Accept Clustering" && tags != "") {
                                        if ($scope.TckAccepted != 0) {
                                            $scope.TckAccepted--;
                                        }
                                        if ($rootScope.AcceptTck != 0) {
                                            $rootScope.AcceptTck--;
                                        }
                                        var TmpJs = { tn: tn, ct1: ct1, ct2: ct2, ct3: ct3, sd: sd, pd: pd, Tags: tags, Disposition: refId, IsTk: 1 };
                                        $rootScope.ActDrillVal.push(TmpJs);
                                    } else if (changes[i][2] == "Send for Re-clustering") {
                                        if ($scope.TckReCluster != 0) {
                                            $scope.TckReCluster--;
                                        }
                                        if ($rootScope.ReClustTck != 0) {
                                            $rootScope.ReClustTck--;
                                        }
                                        var TmpJs = { tn: tn, ct1: ct1, ct2: ct2, ct3: ct3, sd: sd, pd: pd, Tags: tags, Disposition: refId, IsTk: 1 };
                                        $rootScope.SndDrillVal.push(TmpJs);
                                    } else if (changes[i][2] == "Mark as Uncategorized" || (changes[i][2] == "Accept Clustering" && tags == "")) {
                                        if ($scope.TckUnCategorized != 0) {
                                            $scope.TckUnCategorized--;
                                        }
                                        if ($rootScope.UnCatTck != 0) {
                                            $rootScope.UnCatTck--;
                                        }
                                        var TmpJs = { tn: tn, ct1: ct1, ct2: ct2, ct3: ct3, sd: sd, pd: pd, Tags: tags, Disposition: refId, IsTk: 1 };
                                        $rootScope.MarkDrillVal.push(TmpJs);
                                    }
                                    else if (changes[i][2] == "") {
                                        if ($rootScope.NotClustered != 0) {
                                            $rootScope.NotClustered--;
                                        }
                                        var TmpJs = { tn: tn, ct1: ct1, ct2: ct2, ct3: ct3, sd: sd, pd: pd, Tags: tags, Disposition: refId, IsTk: 1 };
                                        $rootScope.NotClusteredVal.push(TmpJs);
                                    }
                                }
                            }
                        }, 10);

                    }
                }
            });
        } else {
            $scope.IsClusterTableAvail = false;
            $scope.IsClusterTableNotAvail = true;
        }
    }    
    function Json2CSV(objArray, ExportFname) {
        var
        getKeys = function (obj) {
            var keys = [];
            for (var key in obj) {
                keys.push(key);
            }
            return keys.join();
        }, array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray
        , str = ''
        ;

        for (var i = 0; i < array.length; i++) {
            var line = '';
            for (var index in array[i]) {
                if (line != '') line += ','

                line += array[i][index];
            }

            str += line + '\r\n';
        }

        str = getKeys(objArray[0]) + '\r\n' + str;

        var a = document.createElement('a');
        var blob = new Blob([str], { 'type': 'application\/octet-stream' });
        a.href = window.URL.createObjectURL(blob);
        a.download = ExportFname + '.xlsx';
        a.click();
        return true;
    }
})
 