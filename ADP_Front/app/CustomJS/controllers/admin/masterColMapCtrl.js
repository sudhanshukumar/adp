appDashboard.controller('MasterColMapController', function ($scope, $routeParams, $rootScope, $http, Notification, $window, mySharedService) {
     $scope.isDMVActive = 3;
	//console.log("= $routeParams.AssessmentId " + $routeParams.AssessmentId);
	$scope.AssessmentId = $routeParams.AssessmentId;	
     /*MasterColMapController related to the _MasterColMap.html for mapping the columns*/

     /*Function Declaration and definition begins*/
     /*Function to save the column mapping settings*/
     $scope.saveMasterCol = function () {
         $scope.savedata = JSON.stringify(makeJSON);

       /*  $http({
                 method: 'POST',
                 url: uriApiURL + 'api/Administrator/SaveMasterColumnMapping?MasterColMapData=' + encodeURIComponent($scope.savedata),
                 headers: {
                     'Content-Type': 'application/x-www-form-urlencoded'
                 }
             })*/
         var url = uriApiURL + 'api/Administrator/SaveMasterColumnMapping?AsgID=' + $scope.AssessmentId;

         var MasterColMap = { 'MasterColMapData': $scope.savedata };

         $http({
             method: 'POST',
             url: url,
             params: { version: getEpocStamp() },
             data: MasterColMap,
             contentType: "application/json",
             dataType: "json"

         }).then(function (response) {
                 //$scope.WholeData = angular.fromJson(response.data);
                 Notification.success({
                     message: 'Changes saved successfully.'
                 });
             });
     }

     /*Function called to reset the columns*/
     $scope.reset = function () {
         $("#masterTable1").handsontable('loadData', $scope.ResetData)
     }

     /*Main Function to get the Master Column Mapping data*/
     $scope.GetMasterColMap = function () {
         var epoTsmp = "?version=" + getEpocStamp();
         $http({
                 method: 'GET',
                 url: uriApiURL + 'api/Administrator/GetMasterColumnMapping?AsgID=' + $scope.AssessmentId,
                 headers: {
                     'Content-Type': 'application/x-www-form-urlencoded'
                 }
             })
             .then(function (response) {
                 $scope.WholeData = angular.fromJson(response.data);
                 $scope.ResetData = angular.fromJson(response.data);
                 $scope.AssessmentName= $scope.WholeData[0].Asg_Name;
                 $scope.GetMasterTable($scope.WholeData);
             });
     }
     $scope.GetTableHeight = function () {
             var setTableHeight = $window.innerHeight - 210;
             return setTableHeight;
         }
         /*Function defining the handsontable*/
     $scope.GetMasterTable = function (data) {
             $('#masterTable1').handsontable({
                 data: data,
                 colHeaders: ['Column Id', 'Column Name', 'Display Column Name', 'Is In Display Table', 'Is In ScoreCarde', 'Is In Filter', 'Is In Pivot','Is In Wordcloud'],
                 rowHeights: 30,
                 columnHeaderHeight: 30,
                 height: $scope.GetTableHeight(),
                 autoColumnSize: {
                     syncLimit: 300
                 },
                 columns: [
                     {
                         data: 'ColID',
                         readOnly: true
                        },
                     {
                         data: 'ColName',
                         readOnly: true
                        },
                     {
                         data: 'DispColName'
                        },
                     {
                         editor: 'select',
                         selectOptions: [0, 1],
                         data: 'IsInRequestMaster',
                         className: "htRight"
					},
                     {
                         editor: 'select',
                         selectOptions: [0, 1],
                         data: 'IsInScoreCard',
                         className: "htRight"
					},
                     {
                         editor: 'select',
                         selectOptions: [0, 1],
                         data: 'IsInFilter',
                         className: "htRight"
					},
                     {
                         editor: 'select',
                         selectOptions: [0, 1],
                         data: 'IsInPivot',
                         className: "htRight"
                     },
                     {
                         editor: 'select',
                        selectOptions: [0, 1],
                        data: 'IsInWordCloud',
                        className: "htRight"
                     }
				],
                 afterChange: function (data, source) {
                     if (source == "edit" || source == "paste" || source == "undo") {
                         var colId = $('#masterTable1').handsontable('getDataAtCell', data[0][0], "ColID");
                         var colName = $('#masterTable1').handsontable('getDataAtCell', data[0][0], "ColName");
                         var dispColName = $('#masterTable1').handsontable('getDataAtCell', data[0][0], "DispColName");
                         var IsInReqMas = $('#masterTable1').handsontable('getDataAtCell', data[0][0], "IsInRequestMaster");
                         var IsInScorecard = $('#masterTable1').handsontable('getDataAtCell', data[0][0], "IsInScoreCard");
                         var IsInFilter = $('#masterTable1').handsontable('getDataAtCell', data[0][0], "IsInFilter");
                         var IsInPivot = $('#masterTable1').handsontable('getDataAtCell', data[0][0], "IsInPivot");
                         var IsInWordCloud = $('#masterTable1').handsontable('getDataAtCell', data[0][0], "IsInWordCloud");
                         var tempJSON = "";
                         if (data[0][2] != data[0][3])
                             if (makeJSON.length > 0) {
                                 makeJSON = makeJSON.filter(function (obj) {
                                     return !(obj.ColID == colId && obj.ColName == colName);
                                 });
                             }

                         if (data[0][1] == "DispColName") {
                             tempJSON = {
                                 "ColID": colId,
                                 "ColName": colName,
                                 "DispColName": data[0][3],
                                 "IsInRequestMaster": IsInReqMas,
                                 "IsInScoreCard": IsInScorecard,
                                 "IsInFilter": IsInFilter,
                                 "IsInPivot": IsInPivot,
                                 "IsInWordCloud":IsInWordCloud
                             };
                         } else if (data[0][1] == "IsInRequestMaster") {
                             tempJSON = {
                                 "ColID": colId,
                                 "ColName": colName,
                                 "DispColName": dispColName,
                                 "IsInRequestMaster": data[0][3],
                                 "IsInScoreCard": IsInScorecard,
                                 "IsInFilter": IsInFilter,
                                 "IsInPivot": IsInPivot,
                                 "IsInWordCloud":IsInWordCloud
                             };
                         } else if (data[0][1] == "IsInScoreCard") {
                             tempJSON = {
                                 "ColID": colId,
                                 "ColName": colName,
                                 "DispColName": dispColName,
                                 "IsInRequestMaster": IsInReqMas,
                                 "IsInScoreCard": data[0][3],
                                 "IsInFilter": IsInFilter,
                                 "IsInPivot": IsInPivot,
                                 "IsInWordCloud": IsInWordCloud
                             };
                         } else if (data[0][1] == "IsInFilter") {
                             tempJSON = {
                                 "ColID": colId,
                                 "ColName": colName,
                                 "DispColName": dispColName,
                                 "IsInRequestMaster": IsInReqMas,
                                 "IsInScoreCard": IsInScorecard,
                                 "IsInFilter": data[0][3],
                                 "IsInPivot": IsInPivot,
                                 "IsInWordCloud": IsInWordCloud
                          
                             };
                         } else if (data[0][1] == "IsInPivot") {
                             tempJSON = {
                                 "ColID": colId,
                                 "ColName": colName,
                                 "DispColName": dispColName,
                                 "IsInRequestMaster": IsInReqMas,
                                 "IsInScoreCard": IsInScorecard,
                                 "IsInFilter": IsInFilter,
                                 "IsInPivot": data[0][3],
                                 "IsInWordCloud": IsInWordCloud
                             };
                         } else if (data[0][1] == "IsInWordCloud") {
                             tempJSON = {
                                 "ColID": colId,
                                 "ColName": colName,
                                 "DispColName": dispColName,
                                 "IsInRequestMaster": IsInReqMas,
                                 "IsInScoreCard": IsInScorecard,
                                 "IsInFilter": IsInFilter,
                                 "IsInPivot": IsInPivot,
                                 "IsInWordCloud": data[0][3]
                             };
                         }
                         makeJSON.push(tempJSON);
                     }
                 }
             });
         }
         /*Function Declaration and definition ends*/
         /*Function call begins*/
     $scope.GetMasterColMap();
     /*Function call ends*/
 })