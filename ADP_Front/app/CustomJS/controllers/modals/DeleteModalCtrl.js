appDashboard.controller('DeleteModalController', function ($scope, $http, $uibModalInstance, mySharedService, Notification) {
    $scope.isDeleteModal = true;
    $scope.AssessmentId = mySharedService.AssessmentId;
    $scope.ok = function () {
        $scope.isDeleteModal = false;
        $http({
            method: 'POST',
params: {version: getEpocStamp()},
            url: $scope.Deleteurl,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        })
            .success(function (response) {
                if ($scope.IsFromSecStep != undefined && $scope.IsFromSecStep == 0) {
                    $scope.$parent.$broadcast("DelPreProcWords", $scope.IsValType);
                    Notification.success({ message: "Deleted successfully" });
                    $uibModalInstance.dismiss('cancel');
                } else if ($scope.IsFromSecStep != undefined && $scope.IsFromSecStep == 1) {                  
                    if (response == "Success") {
                        $scope.$parent.$broadcast("DelAssessment");
                        Notification.success({ message: "Assessment Deleted successfully" });
                        $uibModalInstance.dismiss('cancel');
                    } else {
                        Notification.error({ message: "Sorry! Could not delete the Assessment.. Try after sometime!" });
                        $uibModalInstance.dismiss('cancel');
                    }
                }
                else{
                   var delteIte= '';
					if ($scope.delval == "dashdel") {
						 mySharedService.updateKPIdel($scope.delval, $scope.DelDASHID);
						delteIte = " Dashboard " + $scope.DelDASHID;
 					} else if ($scope.delval == "kpidel") {
						 mySharedService.updateKPIdel($scope.delval, $scope.DelKPIID);
						delteIte = " Widget " + $scope.DelKPIID;
					}else if ($scope.delval == "grpdel") {
							delteIte = " Group " + $scope.DelKPIID;
					}
                    Notification.success({
                        message: 'Selected "' + delteIte + '" deleted successfully.'
                    });
                    $uibModalInstance.dismiss('cancel');
                }
            });
        $scope.isDeleteModal = true;
    };
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
});