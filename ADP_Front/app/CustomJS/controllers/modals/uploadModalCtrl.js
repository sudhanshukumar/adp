﻿appDashboard.controller('UploadModalController', function ($scope, $http, $uibModalInstance, mySharedService, Notification) {
    $scope.DownloadFormat = function () {
        mySharedService.Json2CSV($scope.DownlData, $scope.ExportFname);
    }
    $scope.isUploadDataLoad = true;
    $scope.ok = function () {
        $scope.isUploadDataLoad = false;
        var file = $('#chooseFile').get(0).files[0];
        var formData = new FormData();
        formData.append('file', file);
        if ($scope.IsUpload == 0) {
            $scope.Filename = $scope.AsgFileName;
            var file1 = $("#chooseTimeBook").get(0).files[0];
            formData.append('file', file1);
        } else if (file != undefined && file.name.split('.')[1] != 'csv') {
            Notification.error({
                message: 'Kindly select a file in the given format!'
            });
        }
        else {
            $scope.Filename = file.name.split('.')[0];
        }
        $.ajax({
            type: "POST",
            url: uriApiURL + $scope.urlUpload + $scope.Filename,
            data: formData,
            success: function (response) {
                var data=angular.fromJson(response.data);
                Notification.success({
                    message: 'Upload successful'
                });
                mySharedService.GetCluster($scope.IsUpload,data);
                $uibModalInstance.dismiss('cancel');
                $scope.isUploadDataLoad = true;
            },
            contentType: false,
            processData: false
        });
    };
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
})