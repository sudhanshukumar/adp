appDashboard.controller('QueryBuilderController', ['$scope', '$http', '$rootScope', '$filter', 'Notification', function ($scope, $rootScope, $http, $filter, Notification) {
	$scope.isQueryResult = false;
		$scope.onlyNumbers = /^\d+$/
		var data = '{"group": {"operator": "AND","rules": []}}';
		$scope.$on('setbaseclr', function () {
			$scope.$emit('pingbaseclr', $scope.ATClr)
		})
		if ($scope.isKPISetting == true) {
			$scope.isSetting = true;
			var data1 = $scope.QueryJs;
			data = data1;
		} else {
			data = '{"group": {"operator": "AND","rules": []}}';
		}
		$scope.filter = JSON.parse(data);
		$scope.HeaderName = "Query Builder";

		function htmlEntities(str) {
			return String(str) //.replace(/</g, '&lt;').replace(/>/g, '&gt;');
		}

		function computed(group) {
			if (!group) return "";
			var str = "(";
			var cmt = "";
			for (i = 0; i < group.rules.length; i++) {
				if (group.rules[i].group) {
					str += " <strong>" + group.operator + "</strong> (";
					str += $scope.iterateOverRules(group.rules[i].group.rules);
					str += ")";
				} else {
					if (group.rules[i].fieldType == "Numeric") {
						if (group.rules[i].condition == "between") {
							if (group.rules[i].Startdata == undefined) {
								cmt += "Enter a Start value"
							}
							if (group.rules[i].Enddata == undefined) {
								cmt += "Enter a End value"
							}
						} else {
							if (group.rules[i].data == "") {
								cmt += "Enter a value"
							}
						}
					}
					if (group.rules[i].fieldType == "Date") {

						if (group.rules[i].condition == "between") {
							if (group.rules[i].Startdate == undefined) {
								cmt += "Select a Start date"
							}
							if (group.rules[i].Enddate == undefined) {
								cmt += "Select a End date"
							}
						} else {
							if (group.rules[i].stdate == undefined) {
								cmt += "Select a date"
							}
						}
					}
					if (cmt != "") {
						$scope.comments = cmt;
						$scope.isQueryResult = false;
						break;
					} else {
						str += $scope.iterateOverRules([group.rules[i]]);
					}
				}
			}
			return str + ")";
		}

		$scope.json = null;
		$scope.$on('buildquery', function (e) {
			$scope.comments = "";
			$scope.builderQuery();
			$scope.jsonval = JSON.stringify($scope.filter)
			$scope.$emit("pingqueryBack", $scope.output, $scope.jsonval, $scope.comments);
		});
		$scope.builderQuery = function () {
			$scope.isQueryResult = true;
			var str = computed($scope.filter.group);
			$scope.output = str;
		}
		$scope.iterateOverRules = function (arrayVal) {
			var str = "";
			for (var i = 0; i < arrayVal.length; i++) {
				if (arrayVal[i].AndOrOperator != undefined) {
					str = str + " " + arrayVal[i].AndOrOperator + " ";
				}
				str += arrayVal[i].field.DbName;

				switch (arrayVal[i].fieldType) {
					case "NonNumeric":
						str += " " + htmlEntities(arrayVal[i].Noncondition)
						break;
					case "Numeric":
						if (arrayVal[i].condition != "between") {
							str += " " + htmlEntities(arrayVal[i].condition) + " '" + arrayVal[i].data + "'"
						} else {
							str += " " + htmlEntities(arrayVal[i].condition) + " '" + arrayVal[i].Startdata + "' and '" + arrayVal[i].Enddata + "'"
						}
						break;
					case "Date":
						if (arrayVal[i].condition != "between") {
							str += " " + htmlEntities(arrayVal[i].condition) + " '" + $filter('date')(arrayVal[i].stdate, "dd-MMM-yyyy") + "'"
						} else {
							str += " " + htmlEntities(arrayVal[i].condition) + " '" + $filter('date')(arrayVal[i].Startdate, "dd-MMM-yyyy") + "' and '" + $filter('date')(arrayVal[i].Enddate, "dd-MMM-yyyy") + "'"
						}
						break;
				}
			}
			return str;
		}
	}])
