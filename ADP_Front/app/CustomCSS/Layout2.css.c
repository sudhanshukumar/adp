a {
    cursor: pointer;
}

html {
    position: relative;
    min-height: 100%;
}

body {
    margin: 0;
    position: relative;
    min-height: 100%;
    padding: 0;
    font-family: 'Segoe UI', Roboto, Helvetica, Sans-Serif !important;
    font-size: 11pt;
}

@media (min-width: 1400px) {
    body {
        font-size: 12pt;
    }
}

@media (min-width: 1800px) {
    body {
        font-size: 13pt;
    }
}

@media (max-width: 1200px) {
    h1 small,
    h2 small,
    h3 small,
    h4 small,
    legend small {
        font-size: 65%;
        margin-left: 5px;
    }
}

.dropdown-menu {
    font-size: 100%;
}

.h5,
h5 {
    font-size: 105%;
}

.h4,
h4 {
    font-size: 110%;
}

@media (min-width: 1400px) {
    .h5,
    h5 {
        font-size: 130%;
    }
    .h4,
    h4 {
        font-size: 160%;
    }
}

.iconstck {
    margin-top: 10px !important;
    margin-left: -30px !important;
}

div.queryRow:only-child div.minus {
    display: none;
}

select.input-sm {
    height: 34px;
}

.psImg {
    width: 32px;
    height: 32px;
    float: right;
}

.psImg1 {
    -webkit-border-radius: 60px;
    -moz-border-radius: 60px;
    border-radius: 60px;
    box-shadow: 0 0 8px rgba(127, 127, 127, 0.8);
    float: left;
    position: absolute;
    background-color: white;
    top: 13px;
}

.cursor {
    cursor: pointer;
}

.c3-tooltip-container {
    z-index: 10000000;
	 box-shadow: 7px 7px 12px -9px #777777;
    border-radius: 8px;
    padding: 5px;
    background-color: #ffffff;
}
.dark  .c3-tooltip-container {
    background-color: #5f6c77;
}
.dark   .c3-tooltip {  
    background-color: #3e4c58;}
.c3-tooltip-container .c3-tooltip{ box-shadow:none}
.dark  .c3-tooltip tr {
    border: 1px solid #8a97a2;
}
.dark
.c3-tooltip td {
    color: #e2e2e2;
	border-left: 1px solid #85929d;
    background-color: #5f6c77;
}.dark
.c3-tooltip th {
    background-color: #3e4c58;
    color: #ffffff;
    border-bottom: 1px solid #b9b9b9;
}
.header {
    height: 60px;
    background-color: #555555 !important;
    width: 100%;
}

.HeaderContentDivider {
    width: 100%;
    height: 0px;
    clear: both;
    box-shadow: 1px 1px 3px #5D5D5D;
}

.headerSettings {
    line-height: 50px;
    height: 60px;
    vertical-align: middle;
	    padding-right: 60px;
}
.headerSettings  a.links {
    padding: 15px 15px;
    line-height: 194%;
    display: block;    float: left;
}
.headerSettings  a.imglinks {
    padding:  0px 0px 0px 15px;
    line-height: 0%;
    display: block;    float: left;
}
.headerSettings  a.imglinks img { width:60px !important; height:60px !important;}
.headerSettings hldr {
    padding: 0;
    margin: 0;
    display: block;
    float: left;
    padding-right: 3px;
    height: 40px;
}

.headerSettings .btn-group {
    margin-left: 0;
    display: block;
    float: left;
    border-left: 1px solid #C0C0C0;
    padding: 0 5px;
    cursor: pointer;
    font-size: 100%;
}

.darkITheme .headerSettings .btn-group {
    border-left: 1px solid rgba(255, 255, 255, 0.2);
    padding: 0 0;
    font-size: 100%;
    color: #DDE2EA;
}

.lightITheme .headerSettings .btn-group {
    border-left: 1px solid rgba(0, 0, 0, 0.1);
    padding: 0 0;
    font-size: 100%;
    color: #333333;
    height: 60px;
}

.headerSettings .btn-group .dropdown-toggle .fa {
    line-height: 50px;
    padding-top: 5px;
}
.headerSettings .btn-group .dropdown-menu > li > a {
    line-height: 30px;
    height: 40px;
    color: white;
    padding: 6px 10px;
    font-size: 100%;
    border-bottom: 1px solid #5C646F;
    
}

.headerSettings .btn-group .dropdown-menu {
    background-color: #3C4A5F;
    margin: 1px -2px 0 0;
    border-radius: 0 0 4px 4px;
}

.headerSettings .btn-group .dropdown-menu > li > a:focus,
.headerSettings .dropdown-menu > li > a:hover {
    color: #E6E6E6;
    background-color: #646D7B;
}

footer {
    position: fixed;
    bottom: 0;
    width: 100%;
    z-index: 22;
    box-shadow: 0 1px 8px 0 rgba(0, 0, 0, .2), 0 3px 4px 0 rgba(0, 0, 0, .14), 0 3px 3px -2px rgba(0, 0, 0, .12);
    border-top: 1px solid #595959;
    padding: 5px;
}

footer.lightITheme {
    background-color: rgb(41, 56, 63) !important;
    border-top: 1px solid rgb(89, 96, 99) !important;
    color: #ffffff !important;
}

footer.darkITheme {
    background-color: rgba(255, 255, 255, 0.8) !important;
    color: #797979 !important;
    border-top: 1px solid #B3B3B3 !important;
}

#lntFooterLogo {
    float: right;
    height: 25px;
    margin-left: 5px;
	    width: 118px;
}

.logoFont {
    font-size: 100% !important;
    font-weight: bold !important;
}

.container .text-muted {
    margin: 20px 0;
}

footer > .container-fluid {
    padding-right: 15px;
    padding-left: 15px;
}

.colorBox {
    display: inline-block;
    height: 30px;
    width: 50px;
    margin: 0 0px;
    padding: 0;
    cursor: pointer;
    box-shadow: 0 0 2px rgba(0, 0, 0, 0.5);
}

.tray {
    min-width: 236px;
    padding: 0 5px;
    
}

.btn-group .dropdown-toggle {
    padding: 0 15px;
}

.dropdown-menu .title {
    font-size: 100%;
}

.dropdown-toggle .caret {
    margin-left: 5px;
}

.dropdown-backdrop {
    background-color: rgba(0, 0, 0, .2);
}

.boxShadow {
    border: 1px solid #D9DADB;
}

.boxShadow:hover {
    box-shadow: 0 0 10px 1px rgb(206, 206, 206);
}

.darkTheme .boxShadow,
.dark .boxShadow {
    border: 1px solid #19212D;
}

.darkTheme .boxShadow:hover,
.dark .boxShadow:hover {
    box-shadow: 0 0 5px 2px rgba(18, 27, 41, 0.5);
}

.btn1:hover {
    box-shadow: 0 3px 5px -1px rgba(127, 127, 127, .2), 0 5px 8px 0 rgba(127, 127, 127, .14), 0 1px 14px 0 rgba(127, 127, 127, .12) !important;
}

.linkvalue {
    cursor: pointer;
    color: #337ab7;
}

.kpi-panel {
    position: absolute;
    width: 100%;
    height: 100%;
    display: block;
}

.kpi-panel.kpi-relative {
    position: relative;
    height: auto;
    vertical-align: top;
}

.kpi-heading {
    height: 40px;
    padding: 5px 0 0 0;
    width: 100%;
    /* text-align: center; */
    position: absolute;
    z-index: 5;
}

.kpi-heading-link {
    padding-right: 5px;    width: 100%;
    text-align: center;
}


.kpi-heading-link a.big {
    padding: 5px 10px 5px 0;
    line-height: 30px;
}

.kpi-title {
    font-size: 115%;
    font-weight: 600;
    white-space: nowrap;
    overflow: hidden;
    -ms-text-overflow: ellipsis;
    -o-text-overflow: ellipsis;
    text-overflow: ellipsis;
    line-height: 30px;
    padding-left: 10px;
    height: 35px;
    width: 100%;
	text-align:center;
}
li.gridster-item  .kpi-title  , li.gridster-item  .kpi-heading-link {
width:auto; max-width:70%;
}
.kpi-title.full {
    width: auto;
}

.KPIDvder {
    width: 100%;
    /*height: 1px;*/
    clear: both;
}

.kpi-content {
    width: 100%;
    margin: 0;
    position: absolute;
    height: 100%;
    padding-top: 40px;
}

.kpi-panel.kpi-relative .kpi-content,
.kpi-panel.kpi-relative .kpi-heading {
    position: relative;
    height: auto;
    padding: 5px 0;
}

.modal-dialog .kpi-panel,
.modal-dialog .kpi-content,
.modal-dialog .kpi-heading {
    position: relative;
    height: auto;
    padding: 5px 0;
}

.KpiH {
    width: 100%;
    margin: 40px 0;
    text-align: center;
    cursor: pointer;
}

.icon-block {}

.modal-header {
    border-bottom: none;
    padding: 10px 15px;
    border-radius: 4px 4px 0 0;
}

.modal-header h3 {
    text-decoration: none;
    font-size: 120%;
    float: left;
}

.modal-header .angularClose {
    margin-top: 0px;
    padding: 0 5px;
    float: right;
    font-size: 200%;
    font-weight: bold;
    line-height: 1;
    cursor: pointer;
}

.modal-footer {
    padding: 5px 15px;
    text-align: right;
    border-top: 1px solid #e5e5e5;
    margin-bottom: 0;
     border-radius:  0 0 4px 4px;
}

.modal-footer.darkTheme {
    border-top: 1px solid #3E4756;
    border-radius: 0 0 0 0;
    margin: 0;
}

.modal-body {
    min-height: 200px;
}

svg text,
svg text tspan {
    font-family: 'Segoe UI', Roboto, Helvetica, Sans-Serif !important;
    font-size: 10pt !important;
    stroke: none !important;
}
.slick-slide .hourheatmap  svg text,
.slick-slide .hourheatmap  svg text tspan {
   
    font-size: 7.5pt !important;
}

.slick-slide .legend rect {
    y: 98;
}
.animate-show {
    -webkit-transition: all linear 10s;
    -moz-transition: all linear 10s;
    -o-transition: all linear 10s;
    transition: all linear 10s;
}

.animate-show.ng-hide {
    opacity: 0;
}

.modal-dialog {
    margin-top: 70px !important;
}
.md-video {     margin: 0 auto !important;
   
    width: 100% !important;
    padding: 10px 50px;}

.btn {
    border-width: 0 !important;
    font-size: 100%;
}

.btn-group .btn {
    border-width: 1px !important;
    font-size: 100%;
}

.ParetoChrt {
    /*height: 65% !important;
	max-height: 65% !important;
	*/
	eight: -moz-calc(100% - 74px);
    height: -webkit-calc(100% - 74px);
    height: calc(100% - 74px);
    
    max-width: 100% !important;
    width: 100% !important;
}

.swContainr + .ParetoChrt {

	height: -moz-calc(100% - 74px);
    height: -webkit-calc(100% - 74px);
    height: calc(100% - 74px);
	/*
    height: 70% !important;
    max-height: 70% !important;
    */
	max-width: 96% !important;
    width: 96% !important;
    margin-left: 2%;
}

@media (min-width: 1400px) {
    .swContainr + .ParetoChrt {
        height: 80% !important;
        max-height: 80% !important;
    }
}

.InVsComChrt,
.AgingChart,
.RespReslnExceedPercent {
    /*height: 70% !important;
	max-height: 70% !important;
	max-width: 96% !important;
	width: 96% !important;
	margin-left: 2%;
	margin-top: 2%;
    height: 85%;
    max-height: 85% !important;*/
	
	height: -moz-calc(100% - 30px);
    height: -webkit-calc(100% - 30px);
    height: calc(100% - 30px);
	
	max-height: -moz-calc(100% - 30px);
    max-height: -webkit-calc(100% - 30px);
    max-height: calc(100% - 30px);
    width: 85%;	
    margin: 0 auto;
}

.swContainr + .InVsComChrt,
.swContainr + .AgingChart,
.swContainr + .RespReslnExceedPercent {    
	
	height: -moz-calc(100% - 40px);
    height: -webkit-calc(100% - 40px);
    height: calc(100% - 40px);
	
	max-height: -moz-calc(100% - 40px);
    max-height: -webkit-calc(100% - 40px);
    max-height: calc(100% - 40px);
    
}
.PivotChart{   
    /*height: 85%;
    max-height: 85% !important;*/
	height: -moz-calc(100% - 40px);
    height: -webkit-calc(100% - 40px);
    height: calc(100% - 40px);
    width: 85%;
    margin: 0 auto;
}

.swContainr + .PivotChart {
	height: -moz-calc(100% - 74px);
    height: -webkit-calc(100% - 74px);
    height: calc(100% - 74px);
	
	}
.AgingChart{   
    height: 85%;
    max-height: 85% !important;
    width: 85%;
    margin: 0 auto;
}
.bgLoading {
    background-color: #000000;
    opacity: 0.6;
    bottom: 0;
    display: block;
    height: 100%;
    left: 0;
    line-height: 100%;
    position: fixed;
    right: 0;
    text-align: center;
    top: 0;
    z-index: 1999;
}

.msg {
    position: absolute;
    top: 45%;
    left: 45%;
    display: block;
    color: white;
    z-index: 2500;
    font-size: 20pt;
    font-weight: 400;
}

.chooseIconFont {
    font-size: 20pt;
    cursor: pointer;
}

.calendarBtn {
    color: #333;
    background-color: #e6e6e6 !important;
    border: 1px solid #ccc !important;
}

.paretoContainer {
    height: 32px;
	font-size:90%;
    width: 96%;
    margin: 0 2%;
     
    white-space: nowrap;
    overflow: hidden;
    -ms-text-overflow: ellipsis;
    -o-text-overflow: ellipsis;
    text-overflow: ellipsis;
    
}
.paretoContainer .form-control { 
    margin: 0;
    height: 22px;
    border: 1px solid rgba(115, 115, 115, 0.5);
    padding: 0 5px;
	padding: 0 5px;
    /*border-bottom-width: 2px !important;*/
    background-color: rgba(115, 115, 115, 0.1);
}
.paretotxtwidth {
    width: 60px;
}

.redrawbtn {
    position: absolute;
    top: 35%;
    left: 45%;
}

.bgParetoChrt {
    position: absolute;
    background-color: #000000;
    opacity: 0.6;
    top: 34px;
}
.modal-content .bgParetoChrt {    
    top: 0px;
}
.boldlbl {
    font-weight: 400;
}

.boldlbl1 {
    font-weight: 500;
}

.grey {
    background-color: #c0c0c0;
}

.white {
    background-color: #ffffff;
}

.reorderCls .gridster {
    z-index: 2000;
    background-color: white !important;
}

.glyphicon {
    font-family: FontAwesome;
    font-style: normal;
    font-weight: normal;
}

.glyphicon-chevron-left:before {
    content: "\f053";
}

.glyphicon-chevron-right:before {
    content: "\f054";
}

.dashbIcon {
    height: 70px;
    border-radius: 50%;
    width: 70px;
    font-size: 38pt !important;
    text-align: center;
    line-height: 70px;
    box-shadow: 0 0 5px rgba(127, 127, 127, 0.3);
    vertical-align: middle;
}

.modal-body .dashbIcon {
    border: 1px solid rgb(205, 209, 215);
    box-shadow: none;
}

.ag-fresh .ag-filter {
    border: 1px solid #000;
    background-color: #777;
}

.ag-filter-value {
    font-weight: normal;
}

uSpl {
    border-bottom: 1px dashed #337ab7;
    text-decoration: none;
    cursor: pointer;
}

.tokenize {
    overflow: hidden;
    height: auto !important;
    margin: 3px;
    padding: 1px 3px;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
    border-radius: 5px;
    float: left;
    white-space: nowrap;
    width: auto;
    border: none;
    cursor: pointer;
}

.txtTransform {
    text-transform: lowercase;
}

.tokenDeleteIcon:hover {
    text-decoration: line-through;
}

.tokenDeleteIcon::after {
    content: "\f00d";
    font-family: "FontAwesome";
    font-weight: bold;
    padding-left: 2px;
}

.tokenize:hover {
    text-decoration: line-through;
}

.checkBoxContainer {
    max-height: 300px;
    overflow-y: auto;
    padding: 5px;
    margin-top: 10px;
    border-top: 1px solid rgba(127, 127, 127, 0.5);
}

.FilterData + .noFilterMsg {
    display: none;
}

.zoomTableData {
    width: 100%;
    margin-top: 20px;
}

.zoomTableData tr:nth-child(2n) {
    background-color: rgba(127, 127, 127, 0.1);
}

.zoomTableData th,
.zoomTableData td {
    border: 1px dotted #8C8C8C;
    padding: 5px 2px 5px 2px;
}

.nopad {
    padding: 0 !important;
}

.BlurClass {
    -webkit-filter: blur(3px);
    -o-filter: blur(3px);
    filter: blur(3px);
    opacity: 0.8;
}

.RSSFeed {
    padding: 1%;
}

.RSSFeed > li > img {
    width: 98%;
    height: auto;
}

.RSSFeed > li > p > img {
    width: 98%;
    height: auto;
}

.introjs-button {
    padding: 0 !important;
}

.introjs-skipbutton {
    padding: 5px 10px !important;
    font-size: inherit !important;
    float: left;
    margin-top: 10px;
}

.actIcon1 {
    font-weight: normal;
    font-size: 30pt !important;
    margin: 15px;
}

.actfloat {
    text-align: right;
}

.kpiGridster {
    display: table;
}

.kpiGridsterFirstChild {
    height: 100%;
    width: 100%;
}

.kpiChildDiv,
.kpiCmnCls {
    height: 100%;
}

.kpiCmnCls {
    position: relative;
}


/*Css for Making Swatches*/

.swContainr {
    /*margin-top: 1%;
	height: 8.5%;*/
    height: 40px;
    margin: 0;
    clear: both;
    display: block;
}

.swContainr .swatchGrp {
    text-align: right;
    /* margin-right: 5px; */
    display: block;
   
    min-height: 40px;
}


/*
.btn1:first-child {
	border-top-left-radius: 2px;
	border-bottom-left-radius: 2px;
}

.btn1:last-child {
	border-top-right-radius: 2px;
	border-bottom-right-radius: 2px;
	border-right: 1px solid;
}
 */

.swatchGrp > .btn1 {
    padding: 3px 13px;
    margin-right: 5px;
    display: inline;
    line-height: 14px;
    cursor: pointer;
    border: 1px solid;
    font-weight: 600;
    border-radius: 15px;
}

.swatchGrp.mini {
    padding-left: 5px;
}

.swatchGrp.mini > .btn1 {
   /* padding: 6px 8px;
    margin-right: 1px;
    display: table-cell;
    line-height: 10pt;
    cursor: pointer;
    border: none;
    font-weight: 600;
    border-radius: 15px;
    font-size: 90%;
    font-weight: normal;*/
}

.btn1.theme-text {
    box-shadow: none;
}

.btn1:not(.theme-text) {
    /*box-shadow: 0 1px 5px 0 rgba(127,127,127,.2),0 2px 2px 0 rgba(127,127,127,.14),0 3px 1px -2px rgba(127,127,127,.12) !important;*/
}


/*Css for Making Swatches*/


/*Css For backFace*/

.priorities {
    display: inline-block;
}

.wid1 {
    width: 100%;
    display: inline-block;
}

.wid2 {
    width: 50%;
    display: inline-block;
}

.wid3 {
    width: 33.3%;
    display: inline-block;
}

.wid4 {
    width: 25%;
    display: inline-block;
}

.sparkLineChart {
    height: 50px;
}

.box-show-setup,
.box-hide-setup {
    -webkit-transition: all linear 0.3s;
    -moz-transition: all linear 0.3s;
    -ms-transition: all linear 0.3s;
    -o-transition: all linear 0.3s;
    transition: all linear 0.3s;
}

.frontPanel .severityHeaderTitle {
    margin-top: 20px;
    text-align: center;
}

.frontPanel .violationsCountResponse .priorityContainerData {
    margin-top: 10px;
    text-align: center;
}

.RespReslnChrt {
    height: 53% !important;
    max-width: 96% !important;
    width: 96% !important;
    margin-left: 2%;
    max-height: 540px  !important;
}

.RespReslnChrt rect {
    cursor: pointer;
    stroke: none;
}

.ams > .ams_btn {
    font-size: 12px;
	color:inherit;
}

.topmarginRes {
    z-index: 10;/* z-index: 10000;*/
}

ul.dark .c3Chart circle,
ul.dark .c3 line,
ul.dark .c3 path {
    stroke: black;
    stroke-width: 2px;
}

ul.light .c3Chart circle,
ul.dark .c3 line,
ul.dark .c3 path {
    stroke: white;
    stroke-width: 2px;
}

.redFont {
    color: #FF0000;
}

.black {}

.incidentVal {
    height: 15%;
    align-items: center;
    display: flex;
    justify-content: flex-end;
}

.kpiFooter {
    height: 10%;
}

.swatchGrp {
    text-align: left;
}


/*Css for Making Swatches*/


/*Css for Incoming Vs Completed*/

.kpiType2 rect,
.kpiType2 circle {
    cursor: pointer;
    stroke: none;
}


/*.InVsComChrt {
	height: 78% !important;
	max-height: 78% !important;
	max-width: 96% !important;
	width: 96% !important;
	margin-left: 2%;
}*/


/*Css for Incoming Vs Completed*/

.gridster-item-resizable-handler.handle-s {
    height: 0;
}

.TblBdy {
    display: table-row;
}

.TblBdy > div > div {
    height: 100%;
    display: flex;
    align-items: center;
}

.ht1 {
    height: 100% !important;
}

.ht2 {
    height: 50% !important;
}

.ht3 > div {
    /*border-right:1px solid #777;*/
    border-top: 1px solid rgba(127, 127, 127, 0.25);
}

.ht4 {
    height: 25% !important;
}

.ht4 > div {
    /*border-right:1px solid #777;*/
    border-top: 1px solid rgba(127, 127, 127, 0.25);
}

.bgLoad {
    height: 100%;
    background: #777777;
    opacity: 0.5;
    position: absolute;
    z-index: 1;
    width: 100%;
}

.darkTheme .bgLoad {
    background: #17202D;
    opacity: 0.8;
}

.bgLoadMessage {
    position: absolute;
    top: 45%;
    left: 35%;
    color: #ffffff;
    z-index: 1;
    font-size: 150%;
    vertical-align: middle;
    line-height: 34px;
    height: 34px;
}

.darkTheme .bgLoadMessage {
    color: #48576F;
}

.bgrLoad {
    height: 100%;
   /* background: rgba(0, 0, 0, 0.2);*/
    position: absolute;
    z-index: 1;
    width: 100%;
    display: table;
}

.bgrLoad.Fixed {
    height: 100%;
    position: fixed;
    width: 100%;
    top: 0;
    left: 0;
    z-index: 5;
}

.darkTheme .bgrLoad {
    background: rgba(0, 0, 0, 0.5);
}

.bgrLoadMessage {
    display: table-cell;
    width: 100%;
    height: 100%;
    vertical-align: middle;
    text-align: center;
    color: #ffffff;
    z-index: 1;
    font-size: 110%;
    vertical-align: middle;
    line-height: 34px;
    height: 34px;
}


.bgLoadMessage .form-control,
.bgrLoadMessage .form-control {
    font-size: 15pt !important;
}

.kpiContainer {
    height: 100%;
}

.KPITblL {
    text-align: left;
    width: 100%;
}

.KPITblR {
    text-align: right;
    width: 100%;
}

.ChrtMsg {
    display: inline-block;
    margin-right: 5px;
}

.KPIDvderF {
    width: 100%;
    height: 2px;
    margin-bottom: 20px;
}

.c3-line {
    stroke-width: 2px;
}



.c3-bar,
.c3-arc {
    stroke-width: 0px;
    fill-opacity: 0.80;
}

.multiselectDiv span button {
    text-overflow: ellipsis;
    overflow: hidden;
    white-space: nowrap;
}

.BLACK {}

.GREY {
    color: #777;
}

.GREYn {
    color: #777;
}

.RED {
    color: #C34548;
}

.GREEN {
    color: green;
}

.YELLOW {
    color: yellow;
}

.splWdgt {
    border-width: 1px !important;
    border-style: solid !important;
}

.splWdgt:hover {
    background-color: inherit !important;
    color: inherit !important;
    border-color: inherit !important;
}

.fltrhd {
    position: absolute;
    right: 0;
    bottom: 0;
    background-color: rgba(45, 54, 68, 0.3);
    z-index: 20;
    width: 100%;
    height: 170px;
}

.lightTheme .fltrhd {
    background-color: rgba(255, 238, 238, 0.3);
}

.radiocolorhd {
    position: absolute;
    right: 0;
    top: 0;
    background-color: rgba(0, 0, 0, 0.3);
    z-index: 20;
    width: 100%;
    height: 60px;
}

.radiocolorhd1 {
    position: absolute;
    right: 0;
    top: 0;
    background-color: rgba(0, 0, 0, 0.3);
    z-index: 20;
    width: 100%;
    height: 25px;
}

.colorhd {
    position: absolute;
    right: 0;
    top: 45px;
    background-color: rgba(240, 242, 245, 0.5);
    z-index: 20;
    width: 100%;
    height: 75px;
}

.darkTheme .colorhd,
.dark .colorhd {
    background-color: rgba(45, 54, 68, 0.8);
}

.table > tbody > tr > td,
.table > tbody > tr > th,
.table > tfoot > tr > td,
.table > tfoot > tr > th,
.table > thead > tr > td,
.table > thead > tr > th {
    border-top: none !important;
}

.chosenFilterContainer {
    max-height: 90px;
    overflow-y: auto;
    /*background-color: white;*/
    border-radius: 4px;
    border: 1px dotted #c0c0c0;
}

.querybuilderContainer {
    max-height: 120px;
    overflow-y: scroll;
    /*background-color: white;*/
    border-radius: 4px;
    border: 1px solid #c0c0c0;
    margin-left: 15px;
}


/*
.iconMD {
	text-shadow: 0 1px 0 rgba(127, 127, 127, 0.70);
}

	.iconMD:hover {
		text-shadow: 0 0 0 rgba(127, 127, 127, 0.70);
	}
*/

.input-sm {
    font-size: inherit;
    padding: 2px 10px;
}

.ams > .ams_btn {
    background-image: none !important;
    padding: 5px 8px;
    min-height: 30px !important;
}

.ag-header-cell {
    font-weight: 500;
}

.no-script-message {
    position: absolute;
    left: 50%;
    top: 50%;
    -webkit-transform: translateX(-50%) translateY(-50%);
    transform: translateX(-50%) translateY(-50%);
    /*background: url('//maps.gstatic.com/tactile/basepage/pegman_sherlock.png') no-repeat;
	background-size: 160px 193px;*/
    font-size: 200%;
    font-weight: 300;
}

.condition {
    margin: 10px 0;
}

.clkIcn {
    padding: 0;
}

.ovr {
    margin-top: 10px;
    margin-bottom: 10px;
}

.GeneravVal {
    font-size: 230%;
}

@media all and (min-width:1400px) {
    .GeneravVal {
        /*font-size: 350%;*/
    }
}

@media all and (min-width:1800px) {
    .GeneravVal {
        font-size: 395%;
    }
}

@media all and (max-width:768px) {
    .GeneravVal {
        font-size: 395%;
    }
}

.ag-fresh .ag-header-cell,
.ag-fresh .ag-root {
    border: 0 !important;
}

.LowRes {
    width: 47.5%;
    display: inline-block;
}

.LowRes:nth-of-type(2n) {
    margin-left: 2%;
    margin-bottom: 20px;
    display: inline-block;
}

@media (max-width: 800px) {
    div.kpi-heading div {
        font-size: 100%;
    }
    .fltrhd {
        height: 340px;
    }
    .predefinedActions {
        min-height: 150px;
    }
    .gridster-mobile .gridster-item.kpiGridster,
    .kpiGridster {
        height: 350px !important;
    }
}

@media all and (min-width:992px) {
    .modal-lg {
        width: 90%;
        margin-left: 5%;
    }
}

@media (max-width: @screen-xs) {
    h1 {
        font-size: 5.9vw;
    }
    body {
        font-size: 10px;
    }
}

@media (max-width: @screen-md) {
    .Generalclass {
        top: 90px;
    }
}

@media (max-width: 480px) {
    .TblHdr,
    .TblBdy .ht3 {
        clear: both !important;
        height: 25% !important;
    }
    .TblBdy > div > div,
    .TblHdr > div {
        padding: 0 5px !important;
    }
    .swatchGrp > .btn1 {
        padding: 1px 3px;
        font-size: 13px;
    }
    .LowRes {
        width: 90%;
        display: inline-block;
    }
    .LowRes:nth-of-type(2n) {
        margin-left: 0;
    }
}

@media (min-width: 481px) and (max-width: 767px) {
    h1 {
        font-size: 26px;
    }
    .Generalclass {
        top: 90px;
    }
    h4 {
        font-size: 15px;
    }
    .LowRes:nth-of-type(2n) {
        margin-left: 0;
    }
    .LowRes {
        width: 90%;
        display: inline-block;
    }
}

@media (min-width: 768px) and (max-width: 979px) {
    h1 {
        font-size: 30px;
    }
    .Generalclass {
        top: 60px;
    }
    h4 {
        font-size: 14px;
    }
    .Generalclass .GeneravVal {
        clear: both;
        text-align: center;
        display: block;
        width: 90%;
        margin: 0 auto;
    }
}

@media (max-width: 991px) {
    .newsFeed {
        display: none;
    }
    .modeDisplay {
        display: none;
    }
}
tags-input .tags 
{
    border-radius:6px;
}
tags-input .tags .tag-item 
{
    background:#00ABA9;
    border-color:#00ABA9;
    color:white;
    border-radius:6px;
}

.slickres{
width:33%; min-height:300px; position:relative; display:block
}
.loginModal{ max-width:300px;}
.loginModal .modal-body {
    min-height: 100px; 
}

.loginModal .modal-footer {
    padding: 0;
    border: none;
}
.loginModal .btn-group
{
    display: block;
    width: 100%;
}
.loginModal .modal-footer .btn {
      width: calc(50% - 1px);
    line-height: 187%;
}

.loginModal .modal-footer 
.btn-group>.btn:first-child:not(:last-child):not(.dropdown-toggle) {
    border-top-left-radius: 0;
}
.loginModal .modal-footer 
.btn-group>.btn:last-child:not(:first-child) {
    border-top-right-radius: 0;
	margin-left:1px;
}