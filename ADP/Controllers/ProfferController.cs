﻿using Microsoft.VisualBasic.FileIO;
using ADP_Common.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Http;
using ADP_Common.Constant;
using System.Data.Entity.Core.EntityClient;
using ADP_Common.Helper;
using Newtonsoft.Json;
using DocumentFormat.OpenXml.Math;
using ADP_Data.EDMX;
using System.Web.Script.Serialization;
using ADP_Data.Repository;
using System.Diagnostics;

namespace ADP_API.Controllers
{
    public class ProfferController : ApiController
    {
        HelperFunctions helper = new HelperFunctions();
        ProfferRepository profferRepo = new ProfferRepository();
        string IsLogin = "";

        [Route("api/Proffer/UploadToValidate")]
        [HttpPost, HttpGet]
        public HttpResponseMessage UploadToValidate(string AsgName, int AsgID, int Cond)
        {
            string[] FilePath = new string[2];            
            string Output = "";



            string UserID = profferRepo.GetUserID(IsLogin);      
       
            try
            {
                FilePath = profferRepo.UploadFile();
                if ((Cond == 0 || Cond == 1) && FilePath.Length != 0)
                {
                     Output = profferRepo.ValidateExcel(FilePath[0], Cond); 
                                      
                }
                return Request.CreateResponse(HttpStatusCode.OK, Output);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError,"Error");
            }
        }


        [Route("api/Proffer/UploadData")]
        [HttpPost, HttpGet]
        public HttpResponseMessage UploadData(string AsgName, int AsgID, int Cond, int IsStepVal)
        {
            string[] FilePath = new string[2];
            string Res = "";           
            string ErVal = "";
            string FileName = "";
            List<string> coladd = new List<string>();
            List<string> colval = new List<string>();
            DataTable dt = new DataTable();
            DataTable dt1 = new DataTable();
            string UserID = profferRepo.GetUserID(IsLogin);
            string Query = "";
            DataSet ds = new DataSet();
            try
            {
                FilePath = profferRepo.UploadFile();
                if ((Cond == 0) && FilePath.Length != 0)
                {                  
                  colval.Add(UserID);
                    dt = profferRepo.ReadExcel(FilePath[0], AsgID, UserID, 2);
                    DataView view = new DataView(dt);
                    dt = view.ToTable("Selected", false, "ASG_ID", "ASG_CREATED_BY", "REQUEST_ID", "SUMMARY", "DESCRIPTION", "REQUEST_TYPE", "REQUEST_STATUS", "STATUS_STATE", "REQUEST_IMPACT", "REQUEST_URGENCY", "REQUEST_PRIORITY", "PRIORITY_REVISED_COUNT", "CREATED_BY", "REQUESTER_FULL_NAME", "REQUESTER_COMPANY_ID", "REQUESTER_COMPANY_NAME", "LOCATION", "COUNTRY", "REGION", "RESOLVED_1ST_LINE", "ASSIGNEE", "ASSIGNED_GROUP", "ASSIGNEE_GROUP_COMPETENCE", "ASSIGN_REVISED_COUNT", "ASSIGNED_SERVICE_DESK", "SUPPORT_TIER", "CREATED_TIMESTAMP", "RESPONSE_TIMESTAMP", "RESOLUTION_TIMESTAMP", "CLOSED_TIMESTAMP", "LAST_MODIFIED_TIMESTAMP", "LAST_ACTIONED_TIMESTAMP", "AREA_ID", "AREA_NAME", "SERVICE_CATEGORY_TIER_1", "SERVICE_CATEGORY_TIER_2", "SERVICE_CATEGORY_TIER_3", "REQUESTER_CATEGORY", "ROOT_CATEGORY_TIER_1", "ROOT_CATEGORY_TIER_2", "ROOT_CATEGORY_TIER_3", "ROOT_CAUSE", "CI", "TOTAL_ESTIMATED_TIME", "TOTAL_USED_TIME", "SOURCE", "Solution", "CAB_NAME", "CAB_STATUS", "RESPONSE_TIME_IN_MINS", "RESOLUTION_TIME_IN_MINS", "PENDING_REQUESTER_MINS", "PENDING_IT_MINS", "MINUTES_OPEN", "CURRENT_STATUS_TIME_IN_MINS", "IS_Exceeding_Response", "IS_Exceeding_Resolve", "REQUEST_TYPE_CATEGORY", "DUEDATE_TIMESTAMP", "REOPEN_COUNT", "ReOpened_Date");
                    using (MOSAIC_DISCOVERY_Entities context = new MOSAIC_DISCOVERY_Entities())
                    {
                        //int dst = profferRepo.UpdateBulk(dt, "DBOARD_DATA_DETAILS_COMMON_REQUEST");
                       // if (dst != 0)
                       // {
                            ErVal = profferRepo.InsertMasterTbl(dt,UserID, AsgName, AsgID);
                            if (ErVal == "Success")
                            {
                                var Asg = (from x in context.DBOARD_DATA_DETAILS_ASG
                                           where x.ASG_ID == AsgID
                                           select x).FirstOrDefault();
                                Asg.ASG_STEP = 2;
                                CallPythonScript(AsgID);
                            }
                            else if (ErVal == "Duplicates")
                            {
                                Res = "Duplicates";
                            }
                            else
                            {
                                Res = "Error";
                            }
                        
                    }
                }
                else
                {
                    Res = "Error";
                }
                if ((Cond == 1) && FilePath.Length != 0)
                {                            
                    colval.Add(UserID);
                    dt = profferRepo.ReadExcel(FilePath[0], AsgID, UserID, 2);
                    DataView view = new DataView(dt);
                    dt = view.ToTable("Selected", false, "ASG_ID", "REQUEST_ID", "CREATED_BY", "CREATED_NAME", "CREATED_TIMESTAMP", "CREATED_MONTH_MAPPING", "CREATED_WEEK_NUMBER", "EVENT_TYPE", "TIME_SPENT", "GROUP_TIME_SPENT");
                    int dst = profferRepo.UpdateBulk(dt1, "DBOARD_DATA_DETAILS_COMMON_REQUEST_TIME");
                    if (dst != 0)
                    {
                        Query = "EXEC INSERT_INTO_MASTER_TABLE '" + UserID + "','" + AsgName + "','" + AsgID + "','" + 1 + "'";
                        ds = helper.GetResultReport(Query);
                        if (ds.Tables.Count != 0)
                        {
                            Res = ds.Tables[0].Rows[0][0].ToString();
                        }
                        else
                        {
                            Res = "Error";
                        }
                    }
                    else
                    {
                        Res = "Error";
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, Res);
            }
            catch (Exception e)
            {
                string msg = "";
                if (e.Message.StartsWith("Violation of PRIMARY KEY constraint"))
                {
                    msg = "Duplicates";
                }
                else
                {
                    msg = "Error";
                }
                return Request.CreateResponse(HttpStatusCode.InternalServerError, msg);
            }
        }
        [Route("api/Proffer/Downloadsample")]
        [HttpPost, HttpGet]
        public HttpResponseMessage Downloadsample(int? Cond)
        {    
            try
            {
                string data= profferRepo.Sampletemplate(Cond);
                return Request.CreateResponse(HttpStatusCode.OK, "Success");
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, "Error");
            }
        }

        [Route("api/Proffer/UploadClusterData")]
        [HttpPost, HttpGet]
        public HttpResponseMessage UploadClusterData(int AsgID, string ClustName)
        {            
            string[] FilePath = new string[3];
            DataTable dt = new DataTable();
            List<string> coladd = new List<string>();
            string UserID = profferRepo.GetUserID(IsLogin);
            try
            {
                using (MOSAIC_DISCOVERY_Entities context = new MOSAIC_DISCOVERY_Entities())
                {
                    FilePath = profferRepo.UploadFile();
                    if (FilePath[0] != "")
                    {
                        dt = profferRepo.ReadFile(FilePath[0], AsgID, coladd, UserID, 3);
                    }
                    string Res = profferRepo.SaveClusterJson(AsgID, dt,1,0);
                    return Request.CreateResponse(HttpStatusCode.OK, Res);
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, "Error");
            }
        }
       
        [Route("api/Proffer/SaveClusterData")]
        [HttpPost,HttpGet]
        public HttpResponseMessage SaveClusterData(List<ClusterSaveJson> ClusterData, int AsgID,int IsResendData)
        {
            try
            {
                string json = JsonConvert.SerializeObject(ClusterData);
                DataTable dt = (DataTable)JsonConvert.DeserializeObject(json, (typeof(DataTable)));
                string Res = profferRepo.SaveClusterJson(AsgID, dt,0, IsResendData);
                return Request.CreateResponse(HttpStatusCode.OK, "S");
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, "Error");
            }
        }

        [Route("api/Proffer/GetAssesmentDetails")]
        [HttpGet, HttpPost]
        public HttpResponseMessage GetAssesmentDetails(int? AsgID,string VerVal, int IsType)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, JsonConvert.SerializeObject(profferRepo.GetAssesmentDetails(AsgID, VerVal, IsType)));
            }
            catch (Exception ex)
            {               
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        [Route("api/Proffer/GetUserAssesmentData")]
        [HttpGet, HttpPost]
        public HttpResponseMessage GetUserAssesmentData()
        {
            List<CategoryChoose> Cluster = new List<CategoryChoose>();
            try
            {
                Cluster = profferRepo.GetUserAssesmentData();
                return Request.CreateResponse(HttpStatusCode.OK, JsonConvert.SerializeObject(Cluster));
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        [Route("api/Proffer/SetAsgDataProc")]
        [HttpGet, HttpPost]
        public HttpResponseMessage SetAsgDataProc(int? AsgID,int Condition,int Type)
        {
            try
            {
                using (MOSAIC_DISCOVERY_Entities context=new MOSAIC_DISCOVERY_Entities())
                {
                    DBOARD_DATA_DETAILS_ASG Asg = context.DBOARD_DATA_DETAILS_ASG.Where(x => x.ASG_ID == AsgID).FirstOrDefault();
                    if (Type == 0)
                    {
                        Asg.ASG_CONDITION = Condition;
                    }
                    else
                    {
                        Asg.ASG_STEP = Condition;
                    }
                    context.SaveChanges();
                    return Request.CreateResponse(HttpStatusCode.OK, "Success");
                }
                    
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }
       
        [Route("api/Proffer/UpdatepyDB")]
        [HttpGet, HttpPost]
        public HttpResponseMessage UpdateStopSynm(ListDelWrds ChangePreWrd)
        {
            int i = 0;
            
                try
            {
                using (MOSAIC_DISCOVERY_Entities context = new MOSAIC_DISCOVERY_Entities())
                {
                    if (ChangePreWrd.StopWords.Count != 0)
                    {
                        string json = JsonConvert.SerializeObject(ChangePreWrd.StopWords);
                        List<DelStopWrds> obj = ChangePreWrd.StopWords;                       
                        if (obj.Count != 0)
                        {
                            foreach (var UpdateList in obj)
                            {
                                                              
                                var AsgCon = (from x in context.DBOARD_DATA_PYTHON_RESPONSE_DATA
                                              where x.Asg_ID == UpdateList.ASG_ID && x.Word == UpdateList.Stop_Word
                                              select x).FirstOrDefault();
                                AsgCon.Remove_Word = 1;                                                 
                                context.SaveChanges();
                            }                          
                        }
                    }
                    if (ChangePreWrd.Synonyms.Count != 0)
                    {
                        string json = JsonConvert.SerializeObject(ChangePreWrd.Synonyms);
                        List<DelSynms> obj = ChangePreWrd.Synonyms;
                        if (obj.Count != 0)
                        {
                            foreach (var UpdateList in obj)
                            {

                                var AsgCon = (from x in context.DBOARD_DATA_PYTHON_RESPONSE_DATA
                                              where x.Asg_ID == UpdateList.ASG_ID && x.Word == UpdateList.Used_Word
                                              select x).FirstOrDefault();
                                AsgCon.Rep_Word = UpdateList.Replacement_Word;
                                context.SaveChanges();
                            }
                        }
                    }

                    return Request.CreateResponse(HttpStatusCode.OK, "Success");
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }
        [Route("api/Proffer/CallPythonScript")]
        [HttpGet, HttpPost]
        public HttpResponseMessage CallPythonScript(int AsgId)
        {          
            
            try
            {
                string Python1 = profferRepo.CallPythonScript(AsgId);
               DataTable PyResp = (DataTable)JsonConvert.DeserializeObject(Python1, (typeof(DataTable)));
              PyResp.Columns.Add("Asg_ID", typeof(System.Int32)).SetOrdinal(0);             
              foreach (DataRow row in PyResp.Rows)
              {
                    row["Asg_ID"] = AsgId;
                }
                profferRepo.UpdateSpecificColBulkpy(AsgId, PyResp, "DBOARD_DATA_PYTHON_RESPONSE_DATA");
                return Request.CreateResponse(HttpStatusCode.OK, "");
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        [Route("api/Proffer/FetechFromDB")]
        [HttpGet, HttpPost]
        public HttpResponseMessage FetechFromDB(int AsgId)
        {
            List<PythonProcess> PythonDisplay = new List<PythonProcess>();
            try
            {
                PythonDisplay = profferRepo.FetchResp(AsgId);
                return Request.CreateResponse(HttpStatusCode.OK, JsonConvert.SerializeObject(PythonDisplay));
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }
        
       
 
        [Route("api/Proffer/AddAsg")]
        [HttpGet, HttpPost]
        public HttpResponseMessage AddAsg(Asgname AsgName)
        {
            try
            {
                string Name = AsgName.AsgName;
                JavaScriptSerializer serializer = new JavaScriptSerializer();             
                int AsgId = profferRepo.AddAsg(Name);
                return Request.CreateResponse(HttpStatusCode.OK, AsgId);
            }
            catch(Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }
        [Route("api/Proffer/SaveAsg")]
        [HttpGet, HttpPost]
        public HttpResponseMessage SaveAsg(int AsgID, PrivacySettings UserData)
        {
            try
            {
                string Jsonval = UserData.UserVal;
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                privacy UserDashboardDt = (privacy)serializer.Deserialize(Jsonval, typeof(privacy));
                return Request.CreateResponse(HttpStatusCode.OK, profferRepo.SaveAsg(AsgID, UserDashboardDt));

            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        [Route("api/Proffer/GetAsgProcDetails")]
        [HttpGet, HttpPost]
        public HttpResponseMessage GetAsgProcDetails(int AsgId)
        {
            try
            {
                StepProgress AsgDt = profferRepo.GetAsgProcDetails(AsgId);
                return Request.CreateResponse(HttpStatusCode.OK, JsonConvert.SerializeObject(AsgDt));
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }
        [Route("api/Proffer/DownloadDatatoCluster")]
        [HttpGet, HttpPost]
        public HttpResponseMessage DownloadDatatoCluster(int AsgId)
        {
            try
            {
                string AsgDt = profferRepo.DownloadDatatoCluster(AsgId);
                return Request.CreateResponse(HttpStatusCode.OK, AsgDt);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }
        [Route("api/Proffer/GetDwnlProcData")]
        [HttpGet,HttpPost]
        public HttpResponseMessage GetDwnlProcData(int AsgId)
        {
            try
            {
                List<DwnlData> AsgDt = profferRepo.GetDwnlProcData(AsgId);
                return Request.CreateResponse(HttpStatusCode.OK, JsonConvert.SerializeObject(AsgDt));
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        [Route("api/Proffer/DelAsg")]
        [HttpGet, HttpPost]
        public HttpResponseMessage DelAsg(int AsgId)
        {
            try
            {
                string AsgDt = profferRepo.DelAsg(AsgId);
                return Request.CreateResponse(HttpStatusCode.OK, AsgDt);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        [Route("api/Proffer/GetDrillDownData")]
        [HttpGet, HttpPost]
        public HttpResponseMessage GetDrillDownData(int AsgId,int Type)
        {
            try
            {
                AssignmentDetails AsgDt = profferRepo.GetDrillDownData(AsgId,Type);
                return Request.CreateResponse(HttpStatusCode.OK, JsonConvert.SerializeObject(AsgDt));
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        [Route("api/Proffer/GetTagName")]
        [HttpGet]
        public HttpResponseMessage GetTagName(int AsgId, string Keyword)
        {
            StringBuilder SearchJson = new StringBuilder();
            try
            {
                List<Search> GetSearchData = new List<Search>();
                GetSearchData = profferRepo.GetTagName(AsgId, Keyword);
                SearchJson.Append("{\"TagDetails\":[");
                foreach (var Fields in GetSearchData)
                {
                    SearchJson.Append("\"" + Fields.SearchValue + "\",");
                }
                if (GetSearchData.Count != 0)
                {
                    SearchJson = SearchJson.Remove(SearchJson.Length - 1, 1);
                }
                SearchJson.Append("]}");
                return Request.CreateResponse(HttpStatusCode.OK, SearchJson);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        [Route("api/Proffer/GetDifClusterVer")]
        [HttpGet]
        public HttpResponseMessage GetDifClusterVer(int AsgId)
        {
            StringBuilder SearchJson = new StringBuilder();
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, JsonConvert.SerializeObject(profferRepo.GetDifClusterVer(AsgId)).ToString());
            }
            catch (Exception ex)
            {
                //errorLoger.LogErrorMessage(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }
       

    }
}
