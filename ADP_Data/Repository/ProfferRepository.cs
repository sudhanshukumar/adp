﻿using ClosedXML.Excel;
using Microsoft.VisualBasic.FileIO;
using Newtonsoft.Json;
using ADP_Common.Helper;
using ADP_Common.Model;
using ADP_Data.EDMX;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.Entity.Core.EntityClient;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using DocumentFormat.OpenXml.Math;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Security.Cryptography;
using System.Security.Principal;
using System.Web;

namespace ADP_Data.Repository
{
    public class ProfferRepository
    {
        HelperFunctions helper = new HelperFunctions();
        string  IsLogin="";
        public virtual List<GetClver> GetDifClusterVer(int AsgId)
        {
            List<GetClver> ver = new List<GetClver>();
            try
            {
                using (MOSAIC_DISCOVERY_Entities context = new MOSAIC_DISCOVERY_Entities())
                {
                    ver = (from x in context.DBOARD_DATA_DETAILS_MAP_ASG_JSON
                           where x.ASG_ID == AsgId
                           select new GetClver
                           {
                               vid = x.VER_ID
                           }).ToList<GetClver>();
                }
            }
            catch (Exception e)
            {

            }
            return ver;
        }
        public virtual Asg_Map GetAssesmentDetails(int? AsgId, string VerVal, int IsType)
        {
            Asg_Map GetMasterData = new Asg_Map();
            try
            {
                using (MOSAIC_DISCOVERY_Entities context = new MOSAIC_DISCOVERY_Entities())
                {
                    context.Database.CommandTimeout = 240;
                    if (IsType == 1)
                    {
                        GetMasterData = (from x in context.DBOARD_DATA_DETAILS_MAP_ASG_JSON
                                         orderby x.ASG_RELATED_ID descending
                                         where x.ASG_ID == AsgId
                                         select new Asg_Map
                                         {
                                             AsgId = x.ASG_ID,
                                             AsgJson = x.ASG_JSON
                                         }).FirstOrDefault();
                    }
                    else
                    {
                        GetMasterData = (from x in context.DBOARD_DATA_DETAILS_MAP_ASG_JSON
                                         where x.ASG_ID == AsgId && x.VER_ID==VerVal
                                         select new Asg_Map
                                         {
                                             AsgId = x.ASG_ID,
                                             AsgJson = x.ASG_JSON
                                         }).FirstOrDefault();
                    }
                }
            }
            catch (Exception e)
            {
                throw;
            }
            return GetMasterData;
        }
        public virtual List<CategoryChoose> GetUserAssesmentData()
        {
            List<CategoryChoose> cluster = new List<CategoryChoose>();
              string UserID =GetUserID(IsLogin);
            try
            {
                using (MOSAIC_DISCOVERY_Entities context = new MOSAIC_DISCOVERY_Entities())
                {
                  
                    cluster = (from x in context.DBOARD_DATA_DETAILS_ASG
                               orderby x.ASG_NAME 
                               where x.ASG_CREATED_BY == UserID || x.Is_Shared == 1 || x.User_ID_Map.Contains(UserID)
                               select new CategoryChoose
                               {
                                   value=x.ASG_ID.ToString(),
                                   name=x.ASG_NAME,
                                   ticked=false
                               }).ToList<CategoryChoose>();
                }
            }
            catch (Exception e)
            {

            }
            return cluster;
        }
        public virtual List<PythonProcess> FetchResp(int AsgId)
        {
            List<PythonProcess> PythonDisplay = new List<PythonProcess>();
            try
            {
                using (MOSAIC_DISCOVERY_Entities context = new MOSAIC_DISCOVERY_Entities())
                {                   
                   PythonDisplay = (from x in context.DBOARD_DATA_PYTHON_RESPONSE_DATA
                               where x.Asg_ID == AsgId
                               select new PythonProcess
                               {
                                   Count=x.Count,
                                   Length=x.Length,
                                   Rep_word=x.Rep_Word,
                                   Word=x.Word,
                                   Remove_Word =x.Remove_Word                                   
                               }).ToList<PythonProcess>();
                }
            }
            catch (Exception e)
            {

            }
            return PythonDisplay;
        }
        public virtual string SaveClusterJson(int AsgID,DataTable dt,int IsUpload,int IsResend)
        {
            string Query = "";
            DataSet dsPivot = new DataSet();
            StringBuilder sbJSON = new StringBuilder();
            try
            {
                using (MOSAIC_DISCOVERY_Entities context = new MOSAIC_DISCOVERY_Entities())
                {
                    int UpdtSuc = UpdateColumn(dt, "TEMP_ASG_PROC");

                    if (UpdtSuc != 0)
                    {
                        Query = "EXEC INSERT_UPDATE_CLUSTER_DATA '" + AsgID + "',0," + IsResend;
                        dsPivot = helper.GetResultReport(Query);
                        var AsgCond = (from t in context.DBOARD_DATA_DETAILS_ASG
                                       where t.ASG_ID == AsgID
                                       select t.ASG_CONDITION).FirstOrDefault();
                        var assignmentIds = (from r in dt.AsEnumerable()
                                             select r.Field<string>("Assignments")).Distinct().ToList();
                        sbJSON.Append("{\"WholeData\":[");
                        foreach (var item in assignmentIds.AsEnumerable())
                        {
                            var ReqIdList = (from r in dt.AsEnumerable()
                                             where r.Field<string>("Assignments") == item
                                             select r.Field<string>("Request_Id")).ToList();

                            List<WordCloud> wordcloud = new List<WordCloud>();
                            List<ClusterDetail> clustData = (from q in context.DBOARD_DATA_DETAILS_ASG_MAP_TAG
                                                             join a in context.DBOARD_DATA_DETAILS_REQUEST
                                                             on new { k1 = q.ASG_ID, k2 = q.REQUEST_ID } equals new { k1 = a.ASG_ID, k2 = a.REQUEST_ID }
                                                             where q.ASG_ID == AsgID && ReqIdList.Contains(q.REQUEST_ID) && ReqIdList.Contains(a.REQUEST_ID)
                                                             select new ClusterDetail
                                                             {
                                                                 tn = q.REQUEST_ID,
                                                                 ct1 = a.REQUESTER_CATEGORY,
                                                                 ct2 = a.ROOT_CATEGORY_TIER_1,
                                                                 ct3 = a.ROOT_CATEGORY_TIER_2,
                                                                 wd = q.PROC_DESCRIPTION,
                                                                 sd = a.DESCRIPTION,
                                                                 pd = q.PROC_DESCRIPTION,
                                                                 asg = q.CLUSTER_ID.ToString(),
                                                                 Tags = q.TAG,
                                                                 Disposition = (IsUpload == 1) ? " " : q.DISPOSITION
                                                             }).ToList<ClusterDetail>();
                            if (clustData.Count != 0)
                            {
                                var t = clustData.Aggregate((i, j) => new ClusterDetail { wd = i.wd + " " + j.wd }).wd;
                                if (t != null)
                                {
                                    string[] source = t.Split(' ');
                                    var distinctWords = (from w in source
                                                         select w.ToLowerInvariant()).Distinct().ToList();
                                    var DataDict = new Dictionary<string, int>();
                                    foreach (string searchTerm in distinctWords)
                                    {
                                        var matchQuery = from word in source
                                                         where word.ToLowerInvariant() == searchTerm.ToLowerInvariant()
                                                         select word;
                                        int wordCount = matchQuery.Count();
                                        wordcloud.Add(new WordCloud() { text = searchTerm, weight = wordCount });
                                    }
                                    var u = wordcloud.OrderByDescending(i => i.weight).ToList().Take(50);
                                    sbJSON.Append("{\"AssignmentId\":\"" + item + "\",\"WordCloud\":" + JsonConvert.SerializeObject(u).ToString() + ",\"ClusterData\":" + JsonConvert.SerializeObject(clustData).ToString() + "},");
                                }
                                else
                                {
                                    sbJSON.Append("{\"AssignmentId\":\"" + item + "\",\"WordCloud\":" + "[]" + ",\"ClusterData\":" + JsonConvert.SerializeObject(clustData).ToString() + "},");
                                }
                            }
                        }

                        sbJSON.Remove(sbJSON.Length - 1, 1);
                        sbJSON.Append("]}");
                    }
                    int vId = context.DBOARD_DATA_DETAILS_MAP_ASG_JSON.Select(x => x.ASG_ID == AsgID).ToList().Count();
                    DBOARD_DATA_DETAILS_MAP_ASG_JSON Asg_js = new DBOARD_DATA_DETAILS_MAP_ASG_JSON();
                    Asg_js.ASG_ID = AsgID;
                    Asg_js.ASG_JSON = sbJSON.ToString();
                    Asg_js.VER_ID = "V" + vId+1;
                    context.DBOARD_DATA_DETAILS_MAP_ASG_JSON.Add(Asg_js);
                    context.SaveChanges();
                    return "Success";
                }
            }
            catch (Exception e)
            {
                return "Error";
            }
        }
        public virtual DataTable ReadExcel(string Filename, int Asg_ID, string UserID, int strtindex)
             {
            DataTable dt = new DataTable();
            try
            {  using (OleDbConnection conn = new OleDbConnection())
                {
                  
                    string Import_FileName = Filename;
                    string fileExtension = Path.GetExtension(Import_FileName);
                    string Uploadedfilename = Path.GetFileName(Import_FileName);
                    string[] sheetname = GetExcelSheetNames(Import_FileName);
                    string ExcelName = sheetname[0];                    
                    if (fileExtension == ".xlsx")
                        conn.ConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + Import_FileName + ";" + "Extended Properties='Excel 12.0 Xml;HDR=YES;'";
                    using (OleDbCommand comm = new OleDbCommand())
                    {                   
                        comm.CommandText = "SELECT * From [" + ExcelName + "]";                      
                        comm.Connection = conn;

                        using (OleDbDataAdapter da = new OleDbDataAdapter())
                        {
                            da.SelectCommand = comm;
                            da.Fill(dt);
                            dt.Columns.Add("ASG_ID").SetOrdinal(0);
                            dt.Columns.Add("ASG_CREATED_BY").SetOrdinal(1);

                            foreach (DataRow d in dt.Rows)
                            {
                                d["ASG_ID"] = Asg_ID;
                                d["ASG_CREATED_BY"] = UserID;
                            }                                                
                        }

                    }
                }
            }
            catch (Exception e)
            {
                throw;
            }
            return dt;
        }
        private String[] GetExcelSheetNames(string excelFile)
        {
            OleDbConnection objConn = null;
            System.Data.DataTable dt = null;

            try
            {
                // Connection String. Change the excel file to the file you
                // will search.
                String connString ="Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + excelFile + ";" + "Extended Properties='Excel 12.0 Xml;HDR=YES;'";
                // Create connection object by using the preceding connection string.
                objConn = new OleDbConnection(connString);
                // Open connection with the database.
               objConn.Open();
                // Get the data table containg the schema guid.
                dt = objConn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);

                if (dt == null)
                {
                    return null;
                }

                String[] excelSheets = new String[dt.Rows.Count];
                int i = 0;

                // Add the sheet name to the string array.
                foreach (DataRow row in dt.Rows)
                {
                    excelSheets[i] = row["TABLE_NAME"].ToString();
                    i++;
                }

                // Loop through all of the sheets if you want too...
                for (int j = 0; j < excelSheets.Length; j++)
                {
                    // Query each excel sheet.
                }

                return excelSheets;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                // Clean up.
                if (objConn != null)
                {
                    objConn.Close();
                    objConn.Dispose();
                }
                if (dt != null)
                {
                    dt.Dispose();
                }
            }
        }
        public virtual DataTable ReadFile(string Filename,int Asg_ID,List<string> coladd, string UserID, int strtindex)
        {
            DataTable dt = new DataTable();
            try
            {
                using (TextFieldParser parser = new TextFieldParser(@"" + Filename + ""))
                {
                    parser.TextFieldType = FieldType.Delimited;
                    parser.SetDelimiters(",");
                    bool firstLine = true;
                    while (!parser.EndOfData)
                    {
                        if (firstLine)
                        {
                            string[] fields = parser.ReadFields();
                            int c = 0;
                            foreach (string field in fields)
                            {
                                if (c == 0)
                                {
                                    dt.Columns.Add("ASG_ID");
                                    dt.Columns.Add("ASG_CREATED_BY");
                                    foreach (var i in coladd)
                                    {
                                        dt.Columns.Add(i);
                                    }
                                }
                                dt.Columns.Add(field);
                                c++;
                            }
                        }
                        else
                        {
                            string[] fields = parser.ReadFields();
                            DataRow dr = dt.NewRow();
                            List<string> RowVal = new List<string>();
                            int i = 2;                          
                            foreach (string field in fields)
                            {
                                dr["ASG_ID"] = Asg_ID;
                                dr["ASG_CREATED_BY"] = UserID;
                                dr[i] = field;
                                i++;
                            }
                            dt.Rows.Add(dr);
                        }
                        firstLine = false;
                    }
                }
            }
            catch(Exception e)
            {
                throw;
            }
            return dt;
        }
        public virtual string[] UploadFile(int filecount=0)
        {
            string filename = "";
            string sPath = "";                
            try
            {
                sPath = ConfigurationManager.AppSettings["filePath"].ToString();
                var x = System.Web.HttpContext.Current.Request.Files;
                System.Web.HttpFileCollection hfc = System.Web.HttpContext.Current.Request.Files;
                string[] fileN = new string[hfc.Count];
                string[] FilePath = new string[hfc.Count];
                for (int iCnt = 0; iCnt <= hfc.Count - 1; iCnt++)
                {
                    System.Web.HttpPostedFile hpf = hfc[iCnt];
                    filename = hpf.FileName;
                    filename = hpf.FileName.Replace(" ", "");
                    fileN[iCnt] = filename;
                    if (hpf.ContentLength > 0)
                    {
                        if (File.Exists(sPath + filename))
                        {
                            hpf.SaveAs(sPath + filename);
                        }
                        else
                        {
                            hpf.SaveAs(sPath + filename);
                        }                        
                    }
                }
                for (int i = 0; i < fileN.Length; i++)
                {
                    FilePath[i] = sPath + fileN[i];
                }                
                return FilePath;
            }
            catch(Exception e)
            {
                throw;
            }
        }
        public virtual int UpdateBulk(DataTable dt,string DestTbl)
        {
            SqlConnection con;
            try
            {
                string s = ADP_Common.Constant.Constants.ConnectionString;
                EntityConnectionStringBuilder e = new EntityConnectionStringBuilder(s);
                string ProviderConnectionString = e.ProviderConnectionString;
                con = new SqlConnection(ProviderConnectionString);
                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(con))
                {
                    bulkCopy.DestinationTableName = ""+ DestTbl+"";
                    bulkCopy.BulkCopyTimeout = 5000;
                    con.Open();
                    bulkCopy.WriteToServer(dt);
                    con.Close();
                    return 1;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public virtual int UpdateColumn(DataTable dt, string DestTbl)
        {
            SqlConnection con;
            try
            {
                string s = ADP_Common.Constant.Constants.ConnectionString;
                EntityConnectionStringBuilder e = new EntityConnectionStringBuilder(s);
                string ProviderConnectionString = e.ProviderConnectionString;
                con = new SqlConnection(ProviderConnectionString);
                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(con))
                {
                    bulkCopy.DestinationTableName = "" + DestTbl + "";
                    bulkCopy.BulkCopyTimeout = 5000;
                    try
                    {
                        if (!(dt.Columns.Contains("DISPOSITION")))
                        {
                            dt.Columns.Add("DISPOSITION");
                        }
                        con.Open();                        
                        bulkCopy.ColumnMappings.Add("Assignments", "CLUSTER_ID");
                        bulkCopy.ColumnMappings.Add("Disposition", "DISPOSITION");
                        bulkCopy.ColumnMappings.Add("Request_Id", "REQUEST_ID");
                        bulkCopy.ColumnMappings.Add("Proc_Description", "PROC_DESCRIPTION");
                        bulkCopy.ColumnMappings.Add("Tag_Name", "TAG");
                        bulkCopy.ColumnMappings.Add("ASG_ID", "ASG_ID");
                        bulkCopy.WriteToServer(dt);
                        con.Close();
                        return 1;
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        return 0;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public virtual int UpdateSpecificColBulkStopSyn(DataTable dt, string DestTbl,int Type,string AsgId)
        {
            SqlConnection con;
            string sql = "";
            try
            {
                string s = ADP_Common.Constant.Constants.ConnectionString;
                EntityConnectionStringBuilder e = new EntityConnectionStringBuilder(s);
                string ProviderConnectionString = e.ProviderConnectionString;
                con = new SqlConnection(ProviderConnectionString);
                con.Open();
                sql = @"UPDATE DBOARD_DATA_DETAILS_ASG SET ASG_RECLUSTER_CONDITION=0 WHERE ASG_ID=" + AsgId;
                SqlCommand cmd = new SqlCommand(sql, con);
                cmd.CommandTimeout = 1200;
                cmd.ExecuteNonQuery();
                con.Close();
                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(con))
                {
                    bulkCopy.DestinationTableName = "" + DestTbl + "";
                    bulkCopy.BulkCopyTimeout = 5000;
                    try
                    {
                        con.Open();
                        if (Type == 0)
                        {
                            bulkCopy.ColumnMappings.Add("ASG_ID", "ASG_ID");
                            bulkCopy.ColumnMappings.Add("Stop_Word", "Stop_Word");
                            bulkCopy.ColumnMappings.Add("Remove_word", "Remove_Word");
                        }
                        else
                        {
                            bulkCopy.ColumnMappings.Add("ASG_ID", "ASG_ID");
                            bulkCopy.ColumnMappings.Add("Used_Word", "Used_Word");
                            bulkCopy.ColumnMappings.Add("Replacement_Word", "Replacement_Word");
                        }
                        bulkCopy.WriteToServer(dt);
                        con.Close();
                        return 1;
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        return 0;
                    }
                }
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        public virtual int UpdateSpecificColBulkpy(int AsgId,DataTable dt, string DestTbl)
        {
            SqlConnection con;
            try
            {
                string s = ADP_Common.Constant.Constants.ConnectionString;
                string sql = "";
                EntityConnectionStringBuilder e = new EntityConnectionStringBuilder(s);
                string ProviderConnectionString = e.ProviderConnectionString;
                con = new SqlConnection(ProviderConnectionString);
                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(con))
                {
                    bulkCopy.DestinationTableName = "" + DestTbl + "";
                    bulkCopy.BulkCopyTimeout = 5000;
                    try
                    {
                        con.Open();
                        sql = @"delete DBOARD_DATA_PYTHON_RESPONSE_DATA WHERE ASG_ID=" + AsgId;
                        SqlCommand cmd = new SqlCommand(sql, con);
                        cmd.CommandTimeout = 1200;
                        cmd.ExecuteNonQuery();
                        bulkCopy.ColumnMappings.Add("Asg_ID", "Asg_ID");                       
                        bulkCopy.ColumnMappings.Add("Count", "Count");
                        bulkCopy.ColumnMappings.Add("Length", "Length");
                        bulkCopy.ColumnMappings.Add("Rep_word", "Rep_Word");
                        bulkCopy.ColumnMappings.Add("Remove_word", "Remove_Word");                       
                        bulkCopy.ColumnMappings.Add("Word", "Word");
                        bulkCopy.WriteToServer(dt);
                        con.Close();
                        return 1;
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        return 0;
                    }
                }
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        public virtual int DeletePreData(int AsgId,string UserId)
        {
            string sql = "";
            int Res = 0;
            string s = ADP_Common.Constant.Constants.ConnectionString;
            System.Data.EntityClient.EntityConnectionStringBuilder e = new System.Data.EntityClient.EntityConnectionStringBuilder(s);
            string ProviderConnectionString = e.ProviderConnectionString;
            SqlConnection con = new SqlConnection(ProviderConnectionString);
            try
            {
                con.Open();
                sql = @"DELETE FROM DBOARD_DATA_DETAILS_COMMON_REQUEST WHERE ASG_ID=" + AsgId+ "AND ASG_CREATED_BY="+UserId;
                SqlCommand cmd = new SqlCommand(sql, con);
                cmd.CommandTimeout = 1200;
                cmd.ExecuteNonQuery();
                con.Close();
                Res = 0;
            }
            catch (Exception ex)
            {
                Res = 1;
            }
            return Res;
        }
        public virtual DataTable fetchData(string SqlString)
        {
            DataTable dt = new DataTable();
            string s = ADP_Common.Constant.Constants.ConnectionString;
            System.Data.EntityClient.EntityConnectionStringBuilder e = new System.Data.EntityClient.EntityConnectionStringBuilder(s);
            string ProviderConnectionString = e.ProviderConnectionString;
            SqlConnection con = new SqlConnection(ProviderConnectionString);
            try
            {
                con.Open();
                SqlDataAdapter sda = new SqlDataAdapter(SqlString, con);
                sda.Fill(dt);
            }
            catch (Exception ex)
            {
            }
            finally
            {
                con.Close();
            }
            return dt;
        }
        public virtual string updateQuery(DataTable dt,int AsgId)
        {
            string msg = "";
            string s = ADP_Common.Constant.Constants.ConnectionString;
            System.Data.EntityClient.EntityConnectionStringBuilder e = new System.Data.EntityClient.EntityConnectionStringBuilder(s);
            string ProviderConnectionString = e.ProviderConnectionString;
            SqlConnection con = new SqlConnection(ProviderConnectionString);
            try
            {
                SqlCommand cmd = new SqlCommand("UPDATE_PROC_DATA", con);
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 1200;
                cmd.Parameters.AddWithValue("@PROC_DATA", dt);
                cmd.Parameters.AddWithValue("@ASG_ID", AsgId);
                cmd.ExecuteNonQuery();
                msg = "Success";
            }
            catch (Exception ex)
            {
                msg = "Error";
            }
            finally
            {
                con.Close();
            }
            return msg;
        }
        public virtual string InsertMasterTbl(DataTable dt,string UserId,string AsgName, int AsgId)
        {
            string msg = "";
            string s = ADP_Common.Constant.Constants.ConnectionString;
            System.Data.EntityClient.EntityConnectionStringBuilder e = new System.Data.EntityClient.EntityConnectionStringBuilder(s);
            string ProviderConnectionString = e.ProviderConnectionString;
            SqlConnection con = new SqlConnection(ProviderConnectionString);
            try
            {
                SqlCommand cmd = new SqlCommand("INSERT_INTO_MASTER_TABLE", con);
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 400000;
                cmd.Parameters.AddWithValue("@PROC_DATA", dt);
                cmd.Parameters.AddWithValue("@USER_ID", UserId);
                cmd.Parameters.AddWithValue("@ASG_NAME", AsgName);
                cmd.Parameters.AddWithValue("@ASG_ID", AsgId);
                cmd.Parameters.AddWithValue("@COND", 0);
                cmd.ExecuteNonQuery();
                msg = "Success";
            }
            catch (Exception ex)
            {
                DeletePreData(AsgId, UserId);
                if (ex.Message.StartsWith("Violation of PRIMARY KEY constraint"))
                {
                    msg = "Duplicates";
                }
                else
                {
                    msg = "Error";
                }
            }
            finally
            {
                con.Close();
            }
            return msg;
        }
       
        public virtual string CallPythonScript(int AsgId)
        {
            PythonProcess Python = new PythonProcess();
            string myString = "";
            try
            {
                string python = @"C:\Python27\python.exe";
                string myPythonApp = @"C:\inetpub\wwwroot\adp_preprocessordemo.py";
               //string myPythonApp = @"D:\ADP-Python\adp_preprocessorsys.py";
                string y = "PROCESS";
                ProcessStartInfo myProcessStartInfo = new ProcessStartInfo(python);
                myProcessStartInfo.UseShellExecute = false;
                myProcessStartInfo.RedirectStandardOutput = true;
                myProcessStartInfo.Arguments = myPythonApp + " " + AsgId + " " + y;
                Process myProcess = new Process();
                myProcess.StartInfo = myProcessStartInfo;
                myProcess.Start();
                StreamReader myStreamReader = myProcess.StandardOutput;
                myString = myStreamReader.ReadLine();         
                myProcess.WaitForExit();
                myProcess.Close();             

            }
            catch (Exception e)
            {

            }
            return myString;
        }       
        public virtual int AddAsg(string AsgName)
        {
            int AsgDt = 0;
            string UserID =GetUserID(IsLogin);
          
            try
            {
                using (MOSAIC_DISCOVERY_Entities context = new MOSAIC_DISCOVERY_Entities())
                {            
                   
                    DBOARD_DATA_DETAILS_ASG Asg = new DBOARD_DATA_DETAILS_ASG();
                    Asg.ASG_NAME = AsgName;
                    Asg.ASG_CREATED_BY = UserID;
                    Asg.ASG_CREATED_ON = DateTime.Now;
                    Asg.ASG_CONDITION = 0;
                    Asg.Is_Shared = 0;
                    Asg.ASG_STEP = 1;
                    context.DBOARD_DATA_DETAILS_ASG.Add(Asg);
                    context.SaveChanges();
                    AsgDt = (from x in context.DBOARD_DATA_DETAILS_ASG
                                 where x.ASG_NAME == AsgName && x.ASG_CREATED_BY == UserID
                                 select x.ASG_ID).FirstOrDefault();
                    
                }
            }
            catch(Exception e)
            {

            }
            return AsgDt;
        }
        public virtual int SaveAsg(int AsgID, privacy UserDashboardDt)
        {           
            var alltemp = "";
            var allName = "";
            List<UserDetailPrivacysettings> obj = UserDashboardDt.List;
            string temp = "";
            string Name = "";


            if (obj != null)
            {
                if (obj.Count > 1)
                {
                    foreach (UserDetailPrivacysettings a in obj)
                    {

                        temp = a._id.ToString();
                        temp = temp + ",";
                        alltemp = alltemp + temp;
                        temp = "";
                        Name = a.name;
                        Name = Name + ",";
                        allName = allName + Name;
                        Name = "";
                        

                    }
                    alltemp = alltemp.TrimEnd(',');
                    allName = allName.TrimEnd(',');
                }

                else if(obj.Count==1)
                {
                    alltemp = obj[0]._id.ToString();
                    allName = obj[0].name;
                }
                else
                {
                    alltemp = null;
                    allName = null;
                }
               
            }


            try
            {
                using (MOSAIC_DISCOVERY_Entities context = new MOSAIC_DISCOVERY_Entities())
                {
                    string UserID =GetUserID(IsLogin);
                    DBOARD_DATA_DETAILS_ASG Asg = (from x in context.DBOARD_DATA_DETAILS_ASG
                                                   where x.ASG_ID == AsgID
                                                   select x).FirstOrDefault();
                    Asg.ASG_NAME = UserDashboardDt.Name;
                    Asg.Is_Shared = UserDashboardDt.Cond;
                    Asg.User_ID_Map = alltemp;
                    Asg.USER_NAME_MAP = allName;                                
                    context.SaveChanges();                   
                }
            }
            catch (Exception e)
            {

            }
            return AsgID;
        }
        public virtual string DownloadDatatoCluster(int AsgId)
        {
                PythonProcess Python = new PythonProcess();
                string myString = "";
                try
                {
                    string python = @"C:\Python27\python.exe";
                    string myPythonApp = @"C:\inetpub\wwwroot\adp_preprocessordemo.py";
                   // string myPythonApp = @"D:\ADP-Python\adp_preprocessorsys.py";
                    string y = "CLEANSE";
                    ProcessStartInfo myProcessStartInfo = new ProcessStartInfo(python);
                    myProcessStartInfo.UseShellExecute = false;
                    myProcessStartInfo.RedirectStandardOutput = true;
                    myProcessStartInfo.Arguments = myPythonApp + " " + AsgId + " " + y;
                    Process myProcess = new Process();
                    myProcess.StartInfo = myProcessStartInfo;
                    myProcess.Start();
                    StreamReader myStreamReader = myProcess.StandardOutput;
                myString = myStreamReader.ReadLine();
                List<PythonOrder> order = JsonConvert.DeserializeObject<List<PythonOrder>>(myString.ToString());
                myString = JsonConvert.SerializeObject(order);              
                myProcess.WaitForExit();
                    myProcess.Close();
                }
                catch (Exception e)
                {

                }
                return myString;
            }
        public virtual string ValidateExcel(string Filepath, int Cond)
        {
            PythonProcess Python = new PythonProcess();
            string y = "";
            if (Cond==1)
            {
                y = "REQUEST";
            }
            else if(Cond==0)
                    {
                y = "EFFORT";
            }
            string myString = "";
            try
            {
                string python = @"C:\Python27\python.exe";
                string myPythonApp = @"C:\inetpub\wwwroot\ExcelValidatordemo.py"; 
                //string myPythonApp = @"D:\ADP-Python\ExcelValidator.py";
                ProcessStartInfo myProcessStartInfo = new ProcessStartInfo(python);
                myProcessStartInfo.UseShellExecute = false;
                myProcessStartInfo.RedirectStandardOutput = true;
                Filepath = Filepath.Replace("\\\\", "\\");
                myProcessStartInfo.Arguments = myPythonApp + " " + Filepath + " " + y;
                Process myProcess = new Process();
                myProcess.StartInfo = myProcessStartInfo;
                myProcess.Start();
                StreamReader myStreamReader = myProcess.StandardOutput;
                myString = myStreamReader.ReadLine();
                myProcess.WaitForExit();
                myProcess.Close();
            }
            catch (Exception e)
            {

            }
            return myString;
        }
        public virtual List<DwnlData> GetDwnlProcData(int AsgId)
        {
            List<DwnlData> JsonData = new List<DwnlData>();
            try
            {
                using (MOSAIC_DISCOVERY_Entities context = new MOSAIC_DISCOVERY_Entities())
                {
                    var AsgCon = (from x in context.DBOARD_DATA_DETAILS_ASG
                                  where x.ASG_ID == AsgId
                                  select x.ASG_CONDITION).FirstOrDefault();
                    if (AsgCon == 1)
                    {
                        JsonData = (from x in context.DBOARD_DATA_DETAILS_REQUEST
                                    join y in context.DBOARD_DATA_DETAILS_ASG_MAP_TAG
                                    on x.REQUEST_ID equals y.REQUEST_ID
                                    where x.ASG_ID == AsgId && y.ASG_ID == AsgId && y.DISPOSITION == "Send for Re-clustering"
                                    select new DwnlData
                                    {
                                        Request_Id = x.REQUEST_ID,
                                        Description = x.DESCRIPTION.Replace(",", " "),
                                        Proc_Description = y.PROC_DESCRIPTION.Replace(",", " ")
                                    }).ToList<DwnlData>();
                    }
                    else
                    {
                        JsonData = (from x in context.DBOARD_DATA_DETAILS_REQUEST
                                    join y in context.DBOARD_DATA_DETAILS_ASG_MAP_TAG
                                    on x.REQUEST_ID equals y.REQUEST_ID
                                    where x.ASG_ID == AsgId && y.ASG_ID == AsgId 
                                    select new DwnlData
                                    {
                                        Request_Id = x.REQUEST_ID,
                                        Description = x.DESCRIPTION.Replace(",", " "),
                                        Proc_Description = y.PROC_DESCRIPTION.Replace(",", " ")
                                    }).ToList<DwnlData>();
                    }
                }
            }
            catch (Exception e)
            {

            }
            return JsonData;
        }
        public virtual StepProgress GetAsgProcDetails(int AsgId)
        {
            StepProgress Steps = new StepProgress();
            try
            {
                using (MOSAIC_DISCOVERY_Entities context = new MOSAIC_DISCOVERY_Entities())
                {
                    var AsgCond = (from t in context.DBOARD_DATA_DETAILS_ASG
                                   where t.ASG_ID == AsgId
                                   select t.ASG_CONDITION).FirstOrDefault();
                    var AsgDt = (from t in context.DBOARD_DATA_DETAILS_ASG
                                   where t.ASG_ID == AsgId
                                   select t).FirstOrDefault();
                    Steps.AsgName = AsgDt.ASG_NAME;
                    Steps.ActStep = AsgDt.ASG_STEP;
                    Steps.Is_Shared = AsgDt.Is_Shared;
                    Steps.User_ID_Map = AsgDt.User_ID_Map;
                    if (Steps.Is_Shared == 2)
                    {
                        List<UserDetailPrivacysettings> UserPrSet = new List<UserDetailPrivacysettings>();
                        Steps.USER_NAME_MAP = AsgDt.USER_NAME_MAP;
                        List<string> names = AsgDt.USER_NAME_MAP.Split(',').ToList<string>();
                        List<string> idVal = AsgDt.User_ID_Map.Split(',').ToList<string>();
                        for (int i = 0; i < names.Count; i++)
                        {
                            UserPrSet.Add(new UserDetailPrivacysettings()
                            {
                                _id = Convert.ToInt32(idVal[i]),
                                name = names[i].ToString()
                            });
                        }
                        Steps.List = UserPrSet;
                    }

                    else
                    {
                        Steps.User_ID_Map = null;
                    }
                    if (AsgCond == 0)
                    {
                        var UplDone = (from x in context.DBOARD_DATA_DETAILS_REQUEST
                                       where x.ASG_ID == AsgId
                                       select x).FirstOrDefault();
                        if (UplDone != null)
                        {
                            Steps.UploadMaster = true;
                            Steps.AcceptTc = (from r in context.DBOARD_DATA_DETAILS_REQUEST
                                              join grp in context.DBOARD_DATA_DETAILS_ASG_MAP_TAG
                                              on r.REQUEST_ID equals grp.REQUEST_ID
                                              where r.ASG_ID == AsgId && r.ASG_ID == grp.ASG_ID && grp.DISPOSITION == "Accept Clustering" && grp.TAG != "UnCategorized"
                                              select r.REQUEST_ID).Count();
                            Steps.UnCategTck = (from r in context.DBOARD_DATA_DETAILS_REQUEST
                                                join grp in context.DBOARD_DATA_DETAILS_ASG_MAP_TAG
                                                on r.REQUEST_ID equals grp.REQUEST_ID
                                                where r.ASG_ID == AsgId && r.ASG_ID == grp.ASG_ID && grp.DISPOSITION == "Accept Clustering" && grp.TAG == "UnCategorized"
                                                select r.REQUEST_ID).Count();
                            Steps.ReCluster = (from r in context.DBOARD_DATA_DETAILS_REQUEST
                                               join grp in context.DBOARD_DATA_DETAILS_ASG_MAP_TAG
                                               on r.REQUEST_ID equals grp.REQUEST_ID
                                               where r.ASG_ID == AsgId && r.ASG_ID == grp.ASG_ID && grp.DISPOSITION == "Send for Re-clustering"
                                               select r.REQUEST_ID).Count();
                            Steps.NotClustered = (from r in context.DBOARD_DATA_DETAILS_REQUEST
                                               join grp in context.DBOARD_DATA_DETAILS_ASG_MAP_TAG
                                               on r.REQUEST_ID equals grp.REQUEST_ID
                                               where r.ASG_ID == AsgId && r.ASG_ID == grp.ASG_ID && grp.DISPOSITION != "Send for Re-clustering" && grp.DISPOSITION != "Accept Clustering"
                                                  select r.REQUEST_ID).Count();
                            Steps.Total = (from r in context.DBOARD_DATA_DETAILS_REQUEST
                                           where r.ASG_ID == AsgId
                                           select r.REQUEST_ID).Count();
                            
                            
                                var ClusterDone = (from k in context.DBOARD_DATA_DETAILS_ASG_MAP_TAG
                                                   where k.ASG_ID == AsgId
                                                   select k).FirstOrDefault();
                                if ((Steps.ActStep==3 && ClusterDone==null) || ClusterDone!=null)
                                {
                                    Steps.ActTab = 3;
                                }
                                else
                                {
                                    Steps.ActTab = 2;
                                }
                            
                        }
                        else
                        {
                            Steps.UploadMaster = false;
                            Steps.ActTab = 1;
                        }
                    }
                    else
                    {
                        var UplDone = (from x in context.DBOARD_DATA_DETAILS_REQUEST
                                       join grp in context.DBOARD_DATA_DETAILS_ASG_MAP_TAG
                                       on x.REQUEST_ID equals grp.REQUEST_ID
                                       where x.ASG_ID == AsgId && x.ASG_ID == grp.ASG_ID 
                                       select x).FirstOrDefault();
                        if (UplDone != null)
                        {
                            Steps.NotClustered = (from r in context.DBOARD_DATA_DETAILS_REQUEST
                                                  join grp in context.DBOARD_DATA_DETAILS_ASG_MAP_TAG
                                                  on r.REQUEST_ID equals grp.REQUEST_ID
                                                  where r.ASG_ID == AsgId && r.ASG_ID == grp.ASG_ID && grp.DISPOSITION != "Send for Re-clustering" && grp.DISPOSITION != "Accept Clustering"
                                                  select r.REQUEST_ID).Count();
                            Steps.AcceptTc = (from r in context.DBOARD_DATA_DETAILS_REQUEST
                                              join grp in context.DBOARD_DATA_DETAILS_ASG_MAP_TAG
                                              on r.REQUEST_ID equals grp.REQUEST_ID
                                              where r.ASG_ID == AsgId && r.ASG_ID == grp.ASG_ID && grp.DISPOSITION == "Accept Clustering" && grp.TAG != "UnCategorized"
                                              select r.REQUEST_ID).Count();
                            Steps.UnCategTck = (from r in context.DBOARD_DATA_DETAILS_REQUEST
                                                join grp in context.DBOARD_DATA_DETAILS_ASG_MAP_TAG
                                                on r.REQUEST_ID equals grp.REQUEST_ID
                                                where r.ASG_ID == AsgId && r.ASG_ID == grp.ASG_ID && grp.DISPOSITION == "Accept Clustering" && grp.TAG == "UnCategorized"
                                                select r.REQUEST_ID).Count();
                            Steps.ReCluster = (from r in context.DBOARD_DATA_DETAILS_REQUEST
                                               join grp in context.DBOARD_DATA_DETAILS_ASG_MAP_TAG
                                               on r.REQUEST_ID equals grp.REQUEST_ID
                                               where r.ASG_ID == AsgId && r.ASG_ID == grp.ASG_ID && grp.DISPOSITION == "Send for Re-clustering"
                                               select r.REQUEST_ID).Count();
                            Steps.Total = (from r in context.DBOARD_DATA_DETAILS_REQUEST
                                           where r.ASG_ID == AsgId
                                           select r.REQUEST_ID).Count();
                            
                            var ProcDone = (from y in context.DBOARD_COMPUTED_SYSTEM_PREPROC_DATA
                                            where y.ASG_ID == AsgId
                                            select y).FirstOrDefault();
                            if (ProcDone != null)
                            {
                                var ClusterDone = (from k in context.DBOARD_DATA_DETAILS_ASG_MAP_TAG
                                                   where k.ASG_ID == AsgId
                                                   select k).FirstOrDefault();
                                if (ClusterDone != null)
                                {
                                    Steps.ActTab = 3;
                                }
                                else
                                {
                                    Steps.ActTab = 2;
                                }
                            }
                            else
                            {
                                Steps.ActTab = 2;
                            }
                        }
                        else
                        {
                            Steps.ActTab = 1;
                        }
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Steps;
        }
        public virtual AssignmentDetails GetDrillDownData(int AsgId,int Type)
        {
            AssignmentDetails ClustDt = new AssignmentDetails();
            try
            {
                string Wrdresult = "";
                string CondVal = "";
                using (MOSAIC_DISCOVERY_Entities context = new MOSAIC_DISCOVERY_Entities())
                {
                    if (Type != 1 && Type!=3)
                    {
                        if (Type == 0)
                        {
                            CondVal = "Accept Clustering";
                        }
                        else if(Type==2)
                        {
                            CondVal = "Send for Re-clustering";
                        }
                        ClustDt.ClusterData = (from q in context.DBOARD_DATA_DETAILS_ASG_MAP_TAG
                                   join a in context.DBOARD_DATA_DETAILS_REQUEST
                                   on new { k1 = q.ASG_ID, k2 = q.REQUEST_ID } equals new { k1 = a.ASG_ID, k2 = a.REQUEST_ID }
                                   where q.ASG_ID == AsgId && q.DISPOSITION == CondVal &&  q.TAG != "UnCategorized"
                                   select new ClusterDetail 
                                   {
                                       tn = q.REQUEST_ID,
                                       ct1 = a.REQUESTER_CATEGORY,
                                       ct2 = a.ROOT_CATEGORY_TIER_1,
                                       ct3 = a.ROOT_CATEGORY_TIER_2,
                                       sd = a.DESCRIPTION,
                                       pd = q.PROC_DESCRIPTION,
                                       asg = q.CLUSTER_ID.ToString(),
                                       Tags = q.TAG,
                                       Disposition = q.DISPOSITION
                                   }).ToList<ClusterDetail>();
                        var results1 = ClustDt.ClusterData.AsEnumerable().GroupBy(x=>x.pd)
                            .Select(group => new
                            {
                                text = group.Key,
                                weight = group.Count()
                            })
                            .OrderByDescending(group => group.weight);
                        Wrdresult = JsonConvert.SerializeObject(results1).ToString();
                    }
                    else if(Type==1)
                    {
                        ClustDt.ClusterData = (from q in context.DBOARD_DATA_DETAILS_ASG_MAP_TAG
                                   join a in context.DBOARD_DATA_DETAILS_REQUEST
                                   on new { k1 = q.ASG_ID, k2 = q.REQUEST_ID } equals new { k1 = a.ASG_ID, k2 = a.REQUEST_ID }
                                   where q.ASG_ID == AsgId && q.DISPOSITION == "Accept Clustering" && q.TAG== "UnCategorized"
                                   select new ClusterDetail
                                   {
                                       tn = q.REQUEST_ID,
                                       ct1 = a.REQUESTER_CATEGORY,
                                       ct2 = a.ROOT_CATEGORY_TIER_1,
                                       ct3 = a.ROOT_CATEGORY_TIER_2,
                                       sd = a.DESCRIPTION,
                                       pd = q.PROC_DESCRIPTION,
                                       asg = q.CLUSTER_ID.ToString(),
                                       Tags = q.TAG,
                                       Disposition = q.DISPOSITION
                                   }).ToList<ClusterDetail>();
                    }
                    else if (Type == 3)
                    {
                        ClustDt.ClusterData = (from q in context.DBOARD_DATA_DETAILS_ASG_MAP_TAG
                                               join a in context.DBOARD_DATA_DETAILS_REQUEST
                                               on new { k1 = q.ASG_ID, k2 = q.REQUEST_ID } equals new { k1 = a.ASG_ID, k2 = a.REQUEST_ID }
                                               where q.ASG_ID == AsgId && q.DISPOSITION != "Accept Clustering" && q.DISPOSITION != "Send for Re-clustering"
                                               select new ClusterDetail
                                               {
                                                   tn = q.REQUEST_ID,
                                                   ct1 = a.REQUESTER_CATEGORY,
                                                   ct2 = a.ROOT_CATEGORY_TIER_1,
                                                   ct3 = a.ROOT_CATEGORY_TIER_2,
                                                   sd = a.DESCRIPTION,
                                                   pd = q.PROC_DESCRIPTION,
                                                   asg = q.CLUSTER_ID.ToString(),
                                                   Tags = q.TAG,
                                                   Disposition = q.DISPOSITION
                                               }).ToList<ClusterDetail>();
                    }
                    if (ClustDt.ClusterData.Count != 0)
                    {
                        ClustDt.WordCloud = ClustDt.ClusterData.AsEnumerable().GroupBy(x => x.pd)
                                .Select(group => new WordCloud
                                {
                                    text = group.Key,
                                    weight = group.Count()
                                })
                                .OrderByDescending(group => group.weight).ToList<WordCloud>();
                    }
                }
            }
            catch (Exception e)
            {

            }
            return ClustDt;
        }      
        public virtual string DelAsg(int AsgId)
        {
            string msg = "";
            string sql = "";
            string s = ADP_Common.Constant.Constants.ConnectionString;
            System.Data.EntityClient.EntityConnectionStringBuilder e = new System.Data.EntityClient.EntityConnectionStringBuilder(s);
            string ProviderConnectionString = e.ProviderConnectionString;
            SqlConnection con = new SqlConnection(ProviderConnectionString);
            try
            {
                SqlCommand cmd = new SqlCommand("DELETE_ASG", con);
                con.Open();
                cmd.CommandTimeout = 1200;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ASG_ID", AsgId);
                cmd.ExecuteNonQuery();
                msg = "Success";
                con.Close();
            }
            catch (Exception ex)
            {
                msg = "Error";
            }
            return msg;
        }
        public virtual List<Search> GetTagName(int AsgId, string keyword)
        {
            List<Search> BaseKPIs = new List<Search>();
            try
            {
                using (MOSAIC_DISCOVERY_Entities context = new MOSAIC_DISCOVERY_Entities())
                {
                    BaseKPIs = (from customermaster in context.DBOARD_DATA_DETAILS_ASG_MAP_TAG
                                where customermaster.TAG.Contains(keyword) && customermaster.ASG_ID == AsgId
                                select new Search
                                {
                                    SearchValue = customermaster.TAG
                                }).Distinct().Take(5).ToList<Search>();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return BaseKPIs;
        }


          public string GetUserID(string IsLogin)
        {
            string UserId = "";
              
            using (MOSAIC_DISCOVERY_Entities context = new MOSAIC_DISCOVERY_Entities())
            {
                IsLogin = (from x in context.DBOARD_COMPUTED_ADMIN_APPLICATION_SETTING
                              select x.Value).FirstOrDefault();
            }
            if(IsLogin=="1")
            {
                if (HttpContext.Current.Request.Cookies["app_id"] != null)
                {
                    UserId = HttpContext.Current.Request.Cookies["app_id"].Value.ToString();
                    string[] ArrayUserID = UserId.Split('=');
                    UserId = ArrayUserID[1];
                    UserId = helper.Decrypt(UserId, "UserID");
                }
            }
            else
            {
                if (HttpContext.Current.Request.Cookies["UserID"] != null)
                {
                    UserId = HttpContext.Current.Request.Cookies["UserID"].Value.ToString();
                    string[] ArrayUserID = UserId.Split('=');
                    UserId = ArrayUserID[1];
                    UserId = helper.Decrypt(UserId, "UserID");
                }
            }

            return UserId;
        }
        public virtual string Sampletemplate(int? cond)
        {
            // List<> Res = new List<>();
            string Res = "";
            string fname = "";
            List<excelsample> header = new List<excelsample>();
            try
            {
                using (MOSAIC_DISCOVERY_Entities context = new MOSAIC_DISCOVERY_Entities())
                {
                    var Res1 = (from x in context.DBOARD_DATA_DETAILS_HEADER
                                where x.DISPLAY == cond
                                select new excelsample
                                {
                                    Data = x.HEADERS
                                }).ToList<excelsample>();
                    var json = JsonConvert.SerializeObject(Res1);
                   if(cond==1)
                    {
                        fname = "Request_Master";
                    }
                    else
                    {
                        fname = "Effort_Master";
                    }
                    DataTable dt = (DataTable)JsonConvert.DeserializeObject(json.ToString(), (typeof(DataTable)));
                    DataTable transposedTable = GenerateTransposedTable(dt);
                    transposedTable.Columns.RemoveAt(0);
                    using (XLWorkbook wb = new XLWorkbook())
                    {
                        wb.Worksheets.Add(transposedTable, "Sheet1");
                        System.Web.HttpResponse Response = System.Web.HttpContext.Current.Response;
                        Response.Clear();
                        Response.Buffer = true; 
                        Response.Charset = "";
                        Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                        Response.AddHeader("content-disposition", "attachment;filename="+fname+".xlsx");
                        using (MemoryStream MyMemoryStream = new MemoryStream())
                        {
                            wb.SaveAs(MyMemoryStream);
                            MyMemoryStream.WriteTo(Response.OutputStream);
                            Response.Flush();
                            Response.End();
                        }
                    }
                }
            }
            catch(Exception e)
            {
                throw e;
            }
            return Res;
        }
        private DataTable GenerateTransposedTable(DataTable inputTable)
        {
            DataTable outputTable = new DataTable();

            // Add columns by looping rows

            // Header row's first column is same as in inputTable
            outputTable.Columns.Add(inputTable.Columns[0].ColumnName.ToString());

            // Header row's second column onwards, 'inputTable's first column taken
            foreach (DataRow inRow in inputTable.Rows)
            {
                string newColName = inRow[0].ToString();
                outputTable.Columns.Add(newColName);
            }

            // Add rows by looping columns        
            for (int rCount = 1; rCount <= inputTable.Columns.Count - 1; rCount++)
            {
                DataRow newRow = outputTable.NewRow();

                // First column is inputTable's Header row's second column
                newRow[0] = inputTable.Columns[rCount].ColumnName.ToString();
                for (int cCount = 0; cCount <= inputTable.Rows.Count - 1; cCount++)
                {
                    string colValue = inputTable.Rows[cCount][rCount].ToString();
                    newRow[cCount + 1] = colValue;
                }
                outputTable.Rows.Add(newRow);
            }

            return outputTable;
        }
    }
}
